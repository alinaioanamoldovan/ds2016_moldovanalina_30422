package com.company.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Toshiba on 11/11/2016.
 */
public interface ITaxService extends Remote {
    public double computeTax(Car c) throws RemoteException;
    public double computeSellingPrice(Car c) throws RemoteException;
}
