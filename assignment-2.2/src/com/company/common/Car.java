package com.company.common;

import java.io.Serializable;

/**
 * Created by Toshiba on 11/11/2016.
 */
public class Car implements Serializable {
    private int year;
    private int engineSize;
    private double price;

    public Car() {
    }

    public Car(int year, int engineSize, double price) {
        this.year = year;
        this.engineSize = engineSize;
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (year != car.year) return false;
        if (engineSize != car.engineSize) return false;
        return Double.compare(car.price, price) == 0;

    }



    @Override
    public String toString() {
        return "Car{" +
                "year=" + year +
                ", engineSize=" + engineSize +
                ", price=" + price +
                '}';
    }
}
