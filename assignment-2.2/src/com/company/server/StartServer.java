package com.company.server;

import com.company.common.Constant;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Toshiba on 11/11/2016.
 */
public class StartServer {

    public static void main(String[] args) throws RemoteException,AlreadyBoundException
    {
        TaxService f = new TaxService();
        Registry registry = LocateRegistry.createRegistry(Constant.RMI_PORT);
        registry.bind(Constant.RMI_ID, f);

        System.out.println("The server started");

    }
}
