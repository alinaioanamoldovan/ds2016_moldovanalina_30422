package com.company.server;

import com.company.common.Car;
import com.company.common.ITaxService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Toshiba on 11/11/2016.
 */
public class TaxService extends UnicastRemoteObject implements ITaxService {
    protected TaxService() throws RemoteException {
    }

    @Override
    public double computeTax(Car c) throws RemoteException {
        int sum=0;
        if (c.getEngineSize() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }

        if(c.getEngineSize()<=1600) sum=8;
        if((c.getEngineSize() >= 1601) && (c.getEngineSize()<=2000)) sum = 18;
        if((c.getEngineSize() >= 2001) && (c.getEngineSize()<=2600)) sum = 72;
        if((c.getEngineSize() >= 2601) && (c.getEngineSize()<=3000)) sum = 144;
        if(c.getEngineSize() >= 3001) sum = 290;
        return c.getEngineSize() / 200.0 * sum;
    }

    @Override
    public double computeSellingPrice(Car c) throws RemoteException {
        double priceSelling = 0;
        //System.out.print("In functie " + priceSelling);
        priceSelling = c.getPrice() - (c.getPrice()/7.0) * (2015 - c.getYear());
        //System.out.print("In functie" + priceSelling);
        return priceSelling;
    }

}

