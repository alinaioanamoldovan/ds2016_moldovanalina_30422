package com.company.client;

import com.company.common.Car;
import com.company.common.ITaxService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

/**
 * Created by Toshiba on 11/11/2016.
 */
public class ClientView extends JFrame {
    private ITaxService service;
    private JPanel contentPane;
    private JLabel engineSizeLabel;
    private JTextField engineSize;
    private JLabel yearLabel;
    private JTextField year;
    private JLabel priceLabel;
    private JTextField price;
    private JButton btnComputeTax;
    private JButton btnSellingPrice;
    private JLabel resultTax;
    private JLabel resultPrice;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientView frame = new ClientView();
                    Dimension dimension = Toolkit.getDefaultToolkit()
                            .getScreenSize();
                    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
                    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
                    frame.setLocation(x, y);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public ClientView() throws RemoteException
    {
        this.service= RMITaxService.getService();
        setTitle("RMI Tax Service");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 615, 390);
        contentPane=new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        yearLabel = new JLabel("Fabrication year");
        yearLabel.setBounds(10, 11, 120, 20);
        year= new JTextField();
        year.setBounds(120,11,120,20);

        engineSizeLabel= new JLabel("Engine capacity");
        engineSizeLabel.setBounds(10,41, 120, 20);
        engineSize = new JTextField();
        engineSize.setBounds(120,41,120,20);
        priceLabel= new JLabel("Purchasing price");
        priceLabel.setBounds(10, 81, 120, 20);
        price= new JTextField();
        price.setBounds(120,81,120,20);
        btnSellingPrice= new JButton("Compute the selling price");
        btnComputeTax= new JButton("Compute the tax");
        btnComputeTax.setBounds(10, 121, 140, 23);
        btnSellingPrice.setBounds(160,121,190,23);
        resultTax= new JLabel();
        resultTax.setBounds(10,161,300,23);
        resultPrice =  new JLabel();
        resultPrice.setBounds(10,201,300,23);
        contentPane.add(yearLabel);
        contentPane.add(year);
        contentPane.add(engineSizeLabel);
        contentPane.add(engineSize);
        contentPane.add(priceLabel);
        contentPane.add(price);
        contentPane.add(btnSellingPrice);
        contentPane.add(btnComputeTax);
        contentPane.add(resultTax);
        contentPane.add(resultPrice);
        btnComputeTax.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String fabrication = year.getText();
                String capacity = engineSize.getText();
                String priceP = price.getText();
                Car c1 = new Car(Integer.parseInt(fabrication),Integer.parseInt(capacity),Double.parseDouble(priceP));
                if (fabrication.equals("") && capacity.equals("") && priceP.equals("")) {
                    displayErrorMessage("the fields cannot be null");
                }

                if (!fabrication.matches("[0-9]+") && !capacity.matches("[0-9]+") && !priceP.matches("[0-9+]")) {
                    displayErrorMessage("The fields can contain only numbers");
                }
                double tax=0;
                try {
                    tax = service.computeTax(c1);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                String display = "The tax for the car is " + tax;
                resultTax.setText(display);
                resultTax.setVisible(true);

                JOptionPane.showMessageDialog(null,"The tax for the car is " + tax, "Results", JOptionPane.PLAIN_MESSAGE);

            }
        });
      btnSellingPrice.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
              String fabrication = year.getText();
              String capacity = engineSize.getText();
              String priceP = price.getText();
              Car c1 = new Car(Integer.parseInt(fabrication),Integer.parseInt(capacity),Double.parseDouble(priceP));
              if (fabrication.equals("") && capacity.equals("") && priceP.equals("")) {
                  displayErrorMessage("the fields cannot be null");
              }

              if (!fabrication.matches("[0-9]+") && !capacity.matches("[0-9]+") && !priceP.matches("[0-9+]")) {
                  displayErrorMessage("The fields can contain only numbers");
              }
              double tax=0;
           //   System.out.print(tax);
              try {
                  tax = service.computeSellingPrice(c1);
              } catch (RemoteException e1) {
                  e1.printStackTrace();
              }
              String display = "The price for the car is " + tax;
              resultPrice.setText(display);
              resultPrice.setVisible(true);

              JOptionPane.showMessageDialog(null,"The selling price for the car is " + tax, "Results", JOptionPane.PLAIN_MESSAGE);
          }
      });
    }
    public void displayErrorMessage(String message) {
    //    clientView.clear();
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

}
JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblUsername = new JLabel("Username");
		
		loginUsername = new JTextField();
		//loginUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		
		loginPassword = new JPasswordField();
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				String password = loginPassword.getText();
				
				int loginStatus = -1;
				
				try {
					loginStatus = clientService.login(username, password);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				currentUser = username;
				if (loginStatus == 1) {
					lblWelcome.setText("Welcome, " + currentUser);
					((CardLayout)contentPane.getLayout()).show(contentPane, "adminPanel");
				}
				else if (loginStatus == 0) {
					// get packages
					try {
						Packages[] packages = clientService.listAllPackages(currentUser);
						fillPackagesTable(packages);
						
						for (int i = 0; i < packages.length; i++) {
							Packages packageVar = packages[i];
							
							
						}
						
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

					label.setText("Welcome, " + currentUser);
					((CardLayout)contentPane.getLayout()).show(contentPane, "clientPanel");
				}
				
				loginUsername.setText("");
				loginPassword.setText("");
			}
		});
		
		JLabel lblNewClientRegister = new JLabel("New client? Register!");
		lblNewClientRegister.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblNewUsername = new JLabel("New username");
		
		registerUsername = new JTextField();
		registerUsername.setColumns(10);
		
		JLabel lblNewPassword = new JLabel("New Password");
		
		registerPassword = new JTextField();
		registerPassword.setColumns(10);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = registerUsername.getText();
				String password = registerPassword.getText();
				
				int status = 0;
				try {
					status = clientService.register(username, password);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				if (status > 0) {
					currentUser = username;
					((CardLayout)contentPane.getLayout()).show(contentPane, "clientPanel");
					registerUsername.setText("");
					registerPassword.setText("");
				}
			}
		});
		
		
		loginPanel.setLayout(null);
		lblUsername.setBounds(100,100,120,50);
		loginUsername.setBounds(100,100,120,50);
		lblPassword.setBounds(100,100,120,50);
		loginPanel.add(lblUsername);
		loginPanel.add(loginUsername);
		loginPanel.add(lblPassword);
		/*GroupLayout gl_loginPanel = new GroupLayout(loginPanel);
		gl_loginPanel.setHorizontalGroup(
			gl_loginPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_loginPanel.createSequentialGroup()
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_loginPanel.createSequentialGroup()
							.addGap(46)
							.addGroup(gl_loginPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblPassword)
								.addComponent(lblUsername))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_loginPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(loginPassword)
								.addComponent(loginUsername)
								.addComponent(btnLogin))
							.addGap(110)
							.addGroup(gl_loginPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewUsername)
								.addComponent(lblNewPassword)))
						.addGroup(gl_loginPanel.createSequentialGroup()
							.addGap(118)
							.addComponent(lblLogin)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewClientRegister)
						.addComponent(registerUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(registerPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnRegister))
					.addContainerGap(212, Short.MAX_VALUE))
		);
		gl_loginPanel.setVerticalGroup(
			gl_loginPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_loginPanel.createSequentialGroup()
					.addGap(116)
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogin)
						.addComponent(lblNewClientRegister))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(loginUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewUsername)
						.addComponent(registerUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(loginPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewPassword)
						.addComponent(registerPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_loginPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLogin)
						.addComponent(btnRegister))
					.addContainerGap(200, Short.MAX_VALUE))
		);*/
		