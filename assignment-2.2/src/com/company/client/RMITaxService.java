package com.company.client;

import com.company.common.Constant;
import com.company.common.ITaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Toshiba on 11/11/2016.
 */
public class RMITaxService {
    private static ITaxService service;

    public static ITaxService getService(){
        if(service==null){
            Registry registry;
            try {
                registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);
                service = (ITaxService) registry.lookup(Constant.RMI_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return service;
    }

}
