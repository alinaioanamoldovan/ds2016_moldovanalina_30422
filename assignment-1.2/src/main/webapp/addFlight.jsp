<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="model.Users"%>
<%@ page import="model.Flight"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="model.City"%>
<% Users user = (Users)session.getAttribute("user");
    if (user == null) {
        System.out.println("User is null...");
        response.sendRedirect("login.jsp");
        return;
    }

    List<Flight> allFlights = (List)session.getAttribute("allFlights");
    List<Users> allClients = (List)session.getAttribute("allClients");
    List<City> allCities = (List)session.getAttribute("allCities");

    String formMessage = (String)session.getAttribute("form_message");
    session.removeAttribute("form_message");
    // session.removeAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="admin.js"></script>
    <link rel="stylesheet" type="text/css" href="styleClient.css">

    <style>

    </style>
    <title>Admin panel</title>
</head>
<body>
<p>
    <strong>
        <% if (formMessage != null) {
            out.println(formMessage);
        } %>
    </strong>
</p>
<% if (user.isAdmin().equals("admin")) { %>
<button id="log"><a id="logout" href="login.jsp">LOGOUT</a></button>
<h1>Welcome, <%=user.getUsername() %>!</h1>
<h2><a href="admin.jsp">Go back to home page</a></h2>
<h2>Insert flight:</h2>
<form id="addFlight" method="post" action="AdminServlet">
    <input type="hidden" name="form_type" value="insert"/>
    <table>
        <tr>
            <td colspan="2">
                <label for="client_username">Client</label>
                <select name="client_username" id="client_username" required>
                    <% for (Users client : allClients) { %>
                    <option value="<%=client.getUsername() %>"><%=client.getUsername() %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label for="airplaneType">Airplane type</label>
                <input type="text" name="airplaneType" id="airplaneType" required/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="departureCity">Departure City</label>
                <select name="departureCity" id="departureCity" required>
                    <% for (City city : allCities) { %>
                    <option value="<%=city.getName() %>,<%=city.getLatitude() %>,<%=city.getLongitude()%>"><%=city.getName() %></option>
                    <% } %>
                </select>
            </td>
            <td>
                <label for="arrivalCity">Arrival City</label>
                <select name="arrivalCity" id="arrivalCity" required>
                    <% for (City city : allCities) { %>
                    <option value="<%=city.getName() %>,<%=city.getLatitude() %>,<%=city.getLongitude()%>"><%=city.getName() %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label for="departureDate">Departure date</label>
                <input type="date" name="departureDate" id="departureDate" required/>
            </td>
            <td>
                <label for="arrivalDate">Arrival date</label>
                <input type="date" name="arrivalDate" id="arrivalDate" required/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="departureTime">Departure time</label>
                <input type="time" name="departureTime" id="departureTime" required/>
            </td>
            <td>
                <label for="arrivalTime">Arrival time</label>
                <input type="time" name="arrivalTime" id="arrivalTime" required/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Insert" class="button">
                <input type="reset" class="button"></td>
        </tr>
    </table>
</form>

<h2>All flights:</h2>
<table border="1" id="flights">
    <tr>
        <th>Client</th>
        <th>Flight number</th>
        <th>Airplane type</th>
        <th>Departure city</th>
        <th>Departure date</th>
        <th>Departure time</th>
        <th>Arrival city</th>
        <th>Arrival date</th>
        <th>Arrival time</th>
    </tr>
    <% for (Flight flight : allFlights) { %>
    <tr>
          <td><%=flight.getClientUsername() %></td>
          <td><%=flight.getFlightNumber()%></td>
          <td><%=flight.getAirplaneType() %></td>
          <td><%=flight.getDepartureCity()%></td>
          <td><%=flight.getDepartureDate() %></td>
          <td><%=flight.getDepartureTime() %></td>
          <td><%=flight.getArrivalCity() %></td>
          <td><%=flight.getArrivalDate() %></td>
          <td><%=flight.getArrivalTime() %></td>

    </tr>
    <% } %>
</table>
<% } else { %>
<p>You do not have access to this page, because you do not have admin rights.<br>
    <a href="login.jsp">Back to login.</a></p>
<% } %>
</body>
</html>