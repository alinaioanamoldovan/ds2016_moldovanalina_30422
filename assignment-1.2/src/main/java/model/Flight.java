package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Toshiba on 10/30/2016.
 */
@Entity
@Table
public class Flight implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer  flightNumber;
    private String client_username;
    private String airplaneType;
    private String departureCity;
    private String departureLongitude;
    private String departureLatitude;
    private Date departureTime;
    private Date departureDate;
    private String arrivalCity;
    private String arrivalLongitude;
    private String arrivalLatitude;
    private Date arrivalTime;
    private Date arrivalDate;

    public Flight() {
    }

    public Flight(String clientUsername, String airplaneType, String departureCity, String departureLongitude, String departureLatitude, Date departureTime, Date departureDate, String arrivalCity, String arrivalLongitude, String arrivalLatitude, Date arrivalTime, Date arrivalDate) {
        this.client_username = clientUsername;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureLongitude = departureLongitude;
        this.departureLatitude = departureLatitude;
        this.departureTime = departureTime;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalLongitude = arrivalLongitude;
        this.arrivalLatitude = arrivalLatitude;
        this.arrivalTime = arrivalTime;
        this.arrivalDate = arrivalDate;
    }

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getClientUsername() {
        return client_username;
    }

    public void setClientUsername(String clientUsername) {
        this.client_username = clientUsername;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDepartureLongitude() {
        return departureLongitude;
    }

    public void setDepartureLongitude(String departureLongitude) {
        this.departureLongitude = departureLongitude;
    }

    public String getDepartureLatitude() {
        return departureLatitude;
    }

    public void setDepartureLatitude(String departureLatitude) {
        this.departureLatitude = departureLatitude;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getArrivalLongitude() {
        return arrivalLongitude;
    }

    public void setArrivalLongitude(String arrivalLongitude) {
        this.arrivalLongitude = arrivalLongitude;
    }

    public String getArrivalLatitude() {
        return arrivalLatitude;
    }

    public void setArrivalLatitude(String arrivalLatitude) {
        this.arrivalLatitude = arrivalLatitude;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
