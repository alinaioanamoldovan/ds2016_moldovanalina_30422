package controller;

import dao.FlightDao;
import dao.FlightDaoImpl;
import model.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
@WebServlet(name = "AdminServlet",urlPatterns = {"/AdminServlet"})
public class AdminServlet extends HttpServlet {
    Flight flight = new Flight();
    FlightDaoImpl flightDaoImp = new FlightDaoImpl();
    FlightDao fDao;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String formType = request.getParameter("form_type");

        if ("insert".equals(formType)) {
            // get form content
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String client = request.getParameter("client_username");
            String airplaneType = request.getParameter("airplaneType");

            String departureCityRaw = request.getParameter("departureCity");
            String departureTokens[] = departureCityRaw.split(",");
            String departureCity = departureTokens[0];
            String departureLat = departureTokens[1];
            String departureLong = departureTokens[2];

            String arrivalCityRaw = request.getParameter("arrivalCity");
            String arrivalTokens[] = arrivalCityRaw.split(",");
            String arrivalCity = arrivalTokens[0];
            String arrivalLat = arrivalTokens[1];
            String arrivalLong = arrivalTokens[2];

            Date departureDate = null;
            Time departureTime = null;
            Date arrivalDate = null;
            Time arrivalTime = null;
            String timeTokens[];

            try {
                String departureDateString = request.getParameter("departureDate");
                departureDate = dateFormat.parse(departureDateString);

                String departureTimeString = request.getParameter("departureTime");
                timeTokens = departureTimeString.split(":");
                departureTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);

                String arrivalDateString = request.getParameter("arrivalDate");
                arrivalDate = dateFormat.parse(arrivalDateString);

                String arrivalTimeString = request.getParameter("arrivalTime");
                timeTokens = arrivalTimeString.split(":");
                arrivalTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Flight newFlight = new Flight(client, airplaneType,
                    departureCity, departureLong, departureLat, departureTime, departureDate,
                    arrivalCity, arrivalLong, arrivalLat, arrivalTime, arrivalDate);

            HttpSession session = request.getSession(false);

            newFlight = flightDaoImp.addFlight(newFlight);
            if(newFlight.getArrivalCity().equals(newFlight.getDepartureCity()))
            {
                session.setAttribute("form_message","Error: The departure city must not be equal with the arrival city");
                response.sendRedirect("admin.jsp");

            }

            if (newFlight.getFlightNumber() == null) {
                // something went wrong
                session.setAttribute("form_message", "ERROR: Flight was not inserted!.");
                response.sendRedirect("admin.jsp");
            } else {
                session.setAttribute("form_message", "SUCCESS: Flight with nr.=" + newFlight.getFlightNumber() + " was inserted");

                List allFlights = flightDaoImp.getAllFlights();
                session.setAttribute("allFlights", allFlights);

                response.sendRedirect("admin.jsp");
            }

        } else if ("delete".equals(formType)) {

            String delFlightNumber = request.getParameter("del_flight_number");
            HttpSession session = request.getSession(false);


            flightDaoImp.deleteFlight(Integer.parseInt(delFlightNumber));

            List allFlights = flightDaoImp.getAllFlights();
            session.setAttribute("allFlights", allFlights);

            response.sendRedirect("admin.jsp");
        } else if ("update".equals(formType)) {

            // get form content
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String flightNumber = request.getParameter("flightNumber");
            String client = request.getParameter("client");
            String airplaneType = request.getParameter("airplaneType");

            String departureCityRaw = request.getParameter("departureCity");
            String departureTokens[] = departureCityRaw.split(",");
            String departureCity = departureTokens[0];
            String departureLat = departureTokens[1];
            String departureLong = departureTokens[2];

            String arrivalCityRaw = request.getParameter("arrivalCity");
            String arrivalTokens[] = arrivalCityRaw.split(",");
            String arrivalCity = arrivalTokens[0];
            String arrivalLat = arrivalTokens[1];
            String arrivalLong = arrivalTokens[2];

            Date departureDate = null;
            Time departureTime = null;
            Date arrivalDate = null;
            Time arrivalTime = null;
            String timeTokens[];

            try {
                String departureDateString = request.getParameter("departureDate");
                departureDate = dateFormat.parse(departureDateString);

                String departureTimeString = request.getParameter("departureTime");
                timeTokens = departureTimeString.split(":");
                departureTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);

                String arrivalDateString = request.getParameter("arrivalDate");
                arrivalDate = dateFormat.parse(arrivalDateString);

                String arrivalTimeString = request.getParameter("arrivalTime");
                timeTokens = arrivalTimeString.split(":");
                arrivalTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Flight flight = new Flight(client, airplaneType,
                    departureCity, departureLong, departureLat, departureTime, departureDate,
                    arrivalCity, arrivalLong, arrivalLat, arrivalTime, arrivalDate);
            flight.setFlightNumber(Integer.parseInt(flightNumber));

            HttpSession session = request.getSession(false);

            flightDaoImp.updateFlight(flight);

            List allFlights = flightDaoImp.getAllFlights();
            session.setAttribute("allFlights", allFlights);

            response.sendRedirect("admin.jsp");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request,response);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request,response);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
