package controller;

import dao.CityDaoImpl;
import dao.FlightDaoImpl;
import dao.UsersDaoImpl;
import model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    private UsersDaoImpl usersDao = new UsersDaoImpl();
    private FlightDaoImpl flightDao = new FlightDaoImpl();
    private CityDaoImpl cityDao = new CityDaoImpl();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        HttpSession session = request.getSession(false);

        Users user = usersDao.getUser(username);

        if (user == null) {
            session.setAttribute("login_error", "User does not exist! Please try again.");
            response.sendRedirect("login.jsp");
        }
        else if (!password.equals(user.getPassword())) {
            session.setAttribute("login_error", "Wrong password! Please try again.");
            response.sendRedirect("login.jsp");
        } else {
            // Login succeeded
            session.setAttribute("user", user);

            if ((user.isAdmin()).equals("admin")) {
                List allFlights = flightDao.getAllFlights();
                session.setAttribute("allFlights", allFlights);

                List allClients = usersDao.getAllUsers();
                session.setAttribute("allClients", allClients);

                List allCities = cityDao.getAllCities();
                session.setAttribute("allCities", allCities);

                response.sendRedirect("admin.jsp");
            } else {
                List clientFlights = flightDao.getUserFlight(username);
                session.setAttribute("clientFlights", clientFlights);

                response.sendRedirect("client.jsp");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
