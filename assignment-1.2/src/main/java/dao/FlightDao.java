package dao;


import model.Flight;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public interface FlightDao {

    public Flight addFlight(Flight flight);
    public List getAllFlights();
    public void updateFlight(Flight flight);
    public void deleteFlight(Integer flightNumber);
    public List getUserFlight(String clientUsername);
}
