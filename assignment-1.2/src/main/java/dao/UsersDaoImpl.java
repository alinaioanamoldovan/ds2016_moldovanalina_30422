package dao;

import model.Users;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public class UsersDaoImpl implements UsersDao {
    public Users getUser(String username) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        Users user = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("FROM Users WHERE username='"+username+"'");
            user = (Users)query.uniqueResult();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return user;

    }

    public List getAllUsers() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        List clientFlights = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("FROM Users ");
            clientFlights = query.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return clientFlights;
}

}
