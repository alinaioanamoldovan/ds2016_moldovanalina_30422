package dao;


import model.Users;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public interface UsersDao {
    public Users getUser(String username);
    public List  getAllUsers();
}
