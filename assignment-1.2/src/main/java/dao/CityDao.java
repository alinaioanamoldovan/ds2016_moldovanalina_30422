package dao;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public interface CityDao {
    public List  getAllCities();
}
