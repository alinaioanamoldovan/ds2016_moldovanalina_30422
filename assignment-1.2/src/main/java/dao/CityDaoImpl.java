package dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public class CityDaoImpl implements CityDao {

    public List getAllCities() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        List clientFlights = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("FROM City ");
            clientFlights = query.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return clientFlights;
    }
}
