package dao;

import model.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtils;

import java.util.List;

/**
 * Created by Toshiba on 10/30/2016.
 */
public class FlightDaoImpl implements FlightDao {
    private static final Log log = LogFactory.getLog(FlightDaoImpl.class);


    public Flight addFlight(Flight flight) {
        int flightNumber = -1;
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightNumber = (Integer) session.save(flight);
            flight.setFlightNumber(flightNumber);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight;


    }


    public List getAllFlights() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        List clientFlights = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("FROM Flight");
            clientFlights = query.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return clientFlights;
    }




    public void updateFlight(Flight flight) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }


    }

    public void deleteFlight(Integer flightNumber) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Flight flight = (Flight)session.get(Flight.class, flightNumber);
            session.delete(flight);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }


    }

    public List getUserFlight(String clientUsername) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction tx = null;
        List clientFlights = null;

        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("FROM Flight WHERE client_username='"+clientUsername+"'");
            clientFlights = query.list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return clientFlights;

    }
}
