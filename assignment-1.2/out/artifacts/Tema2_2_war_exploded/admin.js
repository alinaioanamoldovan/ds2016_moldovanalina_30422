$( window ).load(function() {
  
	// clear all select drop downs
	$("select").val("");
	
});

$(".flightRow").click(function() {
	var clickedRow = $(this);
});

$(document).on("change", "input[type='checkbox']", function () {
	
    $("input:checkbox").each(function() {
        if ($(this).is(":checked")) {
        	// enable update form
        	var id = $(this).attr("id");
        	$(".update_"+id).prop('readonly', false);
        	$(".update_"+id).prop('required', true);
        	$(".update_btn_"+id).prop('disabled', false);
        	
        } else {
        	var id = $(this).attr("id");
        	$(".update_"+id).prop('readonly', true);
        	$(".update_"+id).prop('required', false);
        	$(".update_btn_"+id).prop('disabled', true);
        }
    });
	
});