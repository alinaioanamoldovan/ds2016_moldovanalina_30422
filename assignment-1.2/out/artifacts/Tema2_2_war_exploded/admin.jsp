<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.Users"%>
<%@ page import="model.Flight"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="model.City"%>
<% Users user = (Users)session.getAttribute("user"); 
	if (user == null) {
		System.out.println("User is null...");
		response.sendRedirect("login.jsp");
		return;
	}

   List<Flight> allFlights = (List)session.getAttribute("allFlights");
   List<Users> allClients = (List)session.getAttribute("allClients");
   List<City> allCities = (List)session.getAttribute("allCities");
   
   String formMessage = (String)session.getAttribute("form_message"); 
   session.removeAttribute("form_message");
   // session.removeAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="admin.js"></script>
	<link rel="stylesheet" type="text/css" href="styleClient.css">

	<style>

	</style>
	<title>Admin panel</title>
</head>
<body>
	<p>
		<strong>
			<% if (formMessage != null) {
				out.println(formMessage);
			} %>
		</strong>
	</p>
	<% if (user.isAdmin().equals("admin")) { %>
		<button id="log"><a id="logout" href="login.jsp">LOGOUT</a></button>
		<h1>Welcome, <%=user.getUsername() %>!</h1>
	    <button><h3><a href="addFlight.jsp">Add a new flight</a></h3></button>
	<h2>All flights:</h2>
	<table border="1" id="flights">
		<tr>
			<th><strong>Update</strong></th>
			<th>Client</th>
			<th>Flight number</th>
			<th>Airplane type</th>
			<th>Departure city</th>
			<th>Departure date</th>
			<th>Departure time</th>
			<th>Arrival city</th>
			<th>Arrival date</th>
			<th>Arrival time</th>
			<th></th>
			<th></th>
		</tr>
		<% for (Flight flight : allFlights) { %>
		<tr class="flightRow" id="flight_<%=flight.getFlightNumber() %>">
			<form id="update_flight_<%=flight.getFlightNumber() %>"  method="post" action="AdminServlet">
				<input type="hidden" name="form_type" value="update"/>
				<td><input type="checkbox" id="<%=flight.getFlightNumber() %>" value="edit"/></td>
				<td><input type="text" name="client" value="<%=flight.getClientUsername() %>" readonly/></td>
				<td><input type="text" name="flightNumber" class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getFlightNumber()%>" readonly /></td>
				<td><input type="text" name="airplaneType" class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getAirplaneType() %>" readonly /></td>
				<td class="readonly"><%=flight.getDepartureCity() %><input type="hidden" name="departureCity"  value="<%=flight.getDepartureCity() %>,<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>"  /></td>
				<td><input type="date" name="departureDate"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getDepartureDate() %>" readonly/></td>
				<td><input type="time" name="departureTime"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getDepartureTime() %>" readonly/></td>
				<td class="readonly"><%=flight.getArrivalCity() %><input type="hidden" name="arrivalCity" value="<%=flight.getArrivalCity() %>,<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongitude() %>"  /></td>
				<td><input type="date" name="arrivalDate"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getArrivalDate() %>" readonly/></td>
				<td><input type="time" name="arrivalTime"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getArrivalTime() %>" readonly/></td>
				<td><input class="update_btn_<%=flight.getFlightNumber() %>" type="submit" value="Update" disabled/></td>
			</form>
			<td>
				<form id="deleteFlight" method="post" action="AdminServlet">
					<input type="hidden" name="form_type" value="delete"/>
					<input type="hidden" name="del_flight_number" id="delFlightNumber" value="<%=flight.getFlightNumber() %>"/>
					<input type="submit" value="Delete" />
				</form>
			</td>
		</tr>
		<% } %>
	</table>
	<% } else { %>
		<p>You do not have access to this page, because you do not have admin rights.<br>
		<a href="login.jsp">Back to login.</a></p>
	<% } %>
</body>
</html>