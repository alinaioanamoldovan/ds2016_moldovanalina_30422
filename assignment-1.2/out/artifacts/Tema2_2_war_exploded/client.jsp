<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.Users"%>
<%@ page import="model.Flight"%>
<%@ page import="java.util.List"%>
<% Users user = (Users)session.getAttribute("user");
	List<Flight> clientFlights = (List)session.getAttribute("clientFlights");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="client.js"></script>
	<link rel="stylesheet" type="text/css" href="styleClient.css">

	<style>

	</style>
	<title>User panel</title>
</head>
<body>
	<button><a id="logout" href="login.jsp">LOGOUT</a></button>
	<h1>Welcome, <%=user.getUsername() %>!</h1>
	<h2>You have <%=clientFlights.size() %> flights:</h2>
	<table border="1" id="flights">
		<tr>
			<th>Flight number</th>
			<th>Airplane type</th>
			<th>Departure city</th>
			<th>Departure time</th>
			<th>Departure date</th>
			<th>Arrival city</th>
			<th>Arrival time</th>
			<th>Arrival date</th>
		</tr>
		<% for (Flight flight : clientFlights) { %>
			<tr>
				<td><%=flight.getFlightNumber() %></td>
				<td><%=flight.getAirplaneType() %></td>
				<td><%=flight.getDepartureCity() %>&nbsp;
					<a href="" onclick="displayTime(<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>); return false;">
					(Check local time)
					</a>
				</td>
				<td><%=flight.getDepartureTime() %></td>
				<td><%=flight.getDepartureDate() %></td>
				<td><%=flight.getArrivalCity() %>&nbsp;
					<a href="" onclick="displayTime(<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongitude() %>); return false;">
					(Check local time)
					</a></td>
				<td><%=flight.getArrivalTime() %></td>
				<td><%=flight.getArrivalDate() %></td>
			</tr>
		<% } %>
	</table>
</body>
</html>