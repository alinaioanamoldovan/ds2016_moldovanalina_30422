<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
   String loginError = (String)session.getAttribute("login_error"); 
   session.removeAttribute("user");
   session.removeAttribute("login_error");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Login</title>
</head>
<body>
	<p class="error">
		<% if (loginError != null) {
			out.println(loginError);
		} %>
	</p>
	<div class="container">
		<section id="content">
			<form id="loginForm" method="post" action="LoginServlet">
				<h1>Login</h1>
				<div>
					<input type="text" id="username" name="username" required/>
				</div>
				<div>
					<input type="password" id="password" name="password" required/>
				</div>
				<div id="buttons">
				<input type="submit" value="Log in" />
				<input type="reset" class="button">
				</div>
           </form>
		</section><!-- content -->
	</div><!-- container -->
	</body>

	<script src="js/index.js"></script>

</html>

