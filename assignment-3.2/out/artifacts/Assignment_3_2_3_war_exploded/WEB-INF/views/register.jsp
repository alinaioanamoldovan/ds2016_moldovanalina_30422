<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Toshiba
  Date: 11/27/2016
  Time: 3:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        table {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }
        body{
            background-color: antiquewhite;
        }
    </style>
</head>
<body>
<div align="center">
    <form:form method="post" action="register" commandName="dvd">
        <table border="0">
            <tr>
                <td colspan="2" align="center"><h2>Add a new DVD</h2></td>
            </tr>
            <tr>
                <td>Title</td>
                <td><form:input path="title" id="title"/></td>
            </tr>
            <tr>
                <td>Year:</td>
                <td><form:input path="year" id="year"/></td>
            </tr>
            <tr>
                <td>Price:</td>
                <td><form:input path="price" year="price"/></td>
            </tr>

            <td colspan="2" align="center"><input type="submit" value="Add" /></td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
