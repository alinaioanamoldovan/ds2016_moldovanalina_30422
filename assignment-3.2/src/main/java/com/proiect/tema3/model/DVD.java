package com.proiect.tema3.model;

import java.io.Serializable;

/**
 * Created by Toshiba on 11/27/2016.
 */
public class DVD implements Serializable {
    private String title;
    private int year;
    private double price;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

