package com.proiect.tema3.controller;

import com.proiect.tema3.model.DVD;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    private RabbitTemplate messageTemplate;
    @RequestMapping("/")
    public String home()
    {
        return "index";
    }
    @RequestMapping(value="/register",method = RequestMethod.GET)
    public String viewRegistration(Map<String,Object> model) {
        DVD dvd = new DVD();
        model.put("dvd",dvd);

        return "register";
    }
    @RequestMapping(value="/register",method = RequestMethod.POST)
    public String processRegistration(@ModelAttribute("dvd") DVD dvd,
                         Map<String,Object> model) {
        //model.addAttribute("dvd",dvd);
        System.out.println("Here"+" "+dvd.getTitle()+" "+dvd.getPrice()+" "+dvd.getYear());
        String message =  "Title = "+dvd.getTitle()+" "+"Year = "+dvd.getYear()+" "+"Price = "+dvd.getPrice();
        messageTemplate.convertAndSend("my.routingkey.1", message != null && !message.isEmpty() ? message : "No message");
        return "registerSuccess";
    }
}
