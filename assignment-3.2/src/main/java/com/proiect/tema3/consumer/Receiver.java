package com.proiect.tema3.consumer;

import com.proiect.tema3.Services.MailService;
import com.proiect.tema3.Services.TextService;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("receiver")
public class Receiver {

    public void receiveMessage(String message) throws IOException {
       System.out.println("Message received: " + message);
        MailService mailService = new MailService("alina.moldovan12@gmail.com","twilight12071994@");
        TextService textService = new TextService();

        System.out.println("Senging mail for " + message);
        mailService.sendMail("alyna_943@yahoo.com","New DVD",message);
        System.out.println("Mail sent");
        System.out.println("Creating text file for " + message);
        textService.createTextFile(message);
        System.out.println("Text file created");
   }

}
