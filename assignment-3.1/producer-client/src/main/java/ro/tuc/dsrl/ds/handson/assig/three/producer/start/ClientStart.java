package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import java.io.IOException;
import java.util.Random;

import common.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);
		Random rand = new Random();
		DVD dvdToSend;

		try {
			for (int i=0;i<5;i++) {
				int year = rand.nextInt((2016-1930) + 1) +1930;

				dvdToSend = new DVD("DVD " + i, year, 15.99);

				queue.writeMessage(dvdToSend);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



}
