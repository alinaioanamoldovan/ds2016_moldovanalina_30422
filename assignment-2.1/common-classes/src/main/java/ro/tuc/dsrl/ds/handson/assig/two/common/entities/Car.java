package ro.tuc.dsrl.ds.handson.assig.two.common.entities;

import java.io.Serializable;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-common-classes
 * @Since: Sep 1, 2015
 * @Description:
 * 	Serializable car, abstraction of a Car.
 */
public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	private int year;
	private int engineCapacity;
	private double price;

	public Car() {
	}

	public Car(int year, int engineCapacity,double price) {
		this.year = year;
		this.engineCapacity = engineCapacity;
		this.price = price;

	}



	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Car car = (Car) o;

		if (year != car.year) return false;
		if (engineCapacity != car.engineCapacity) return false;
		return Double.compare(car.price, price) == 0;

	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = year;
		result = 31 * result + engineCapacity;
		temp = Double.doubleToLongBits(price);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "Car{" +
				"year=" + year +
				", engineCapacity=" + engineCapacity +
				", price=" + price +
				'}';
	}
}
