package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description:
 *	Starting point of the Client application.
 */
public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);
	private ClientStart() {
	}

	public static void main(String[] args) throws IOException {
		ITaxService taxService = null;
		try {
			taxService = Naming.lookup(ITaxService.class, ServerConnection.getInstance());


			Car testCar = new Car(2009, 2000, 12000);
			System.out.println("Tax value: " + taxService.computeTax(testCar));
			System.out.println("Selling price: " + taxService.computeSellingPrice(testCar));



		} catch (IOException e) {
			LOGGER.error("",e);
		}
		finally {
			ServerConnection.getInstance().closeAll();
		}
	}
}
