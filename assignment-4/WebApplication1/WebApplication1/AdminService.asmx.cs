﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for AdminService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminService : System.Web.Services.WebService
    {
        [WebMethod]
        public void addPackage(String sender, String receiver, String name, String description, String sender_city, String destination_city)
        {

            Models.Model2 packageModel = new Models.Model2();

            

            packageModel.packages.Add(new Models.Package
            {
                sender = sender,
                receiver = receiver,
                name = name,
                description = description,
                sender_city=sender_city,
                destination_city = destination_city
            });
            packageModel.SaveChanges();

        }
        [WebMethod]
        public void registerPackageForTracking(int packageId)
        {
            Models.Model2 packageModel = new Models.Model2();
            packageModel.packages.Find(packageId).tracking = true;
            packageModel.SaveChanges();
        }

        [WebMethod]
        public void updatePackageStatus(int package_id, string city)
        {
            Models.Model3 packageTrackingModel = new Models.Model3();

            packageTrackingModel.packageTracking.Add(new Models.PackageTracking
                {
                    package_id = package_id,
                    city = city,
                    time = DateTime.Now
                });

            packageTrackingModel.SaveChanges();
        }

    }
}
