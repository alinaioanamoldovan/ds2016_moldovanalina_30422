﻿

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tema4.package_tracking")]
    public partial class PackageTracking
    {
       
        [Key]
        [Required]
        public int package_id { get; set; }

        [Required]
        [StringLength(45)]
        public string city { get; set; }

        [Required]
        public DateTime time { get; set; }

    }
}

