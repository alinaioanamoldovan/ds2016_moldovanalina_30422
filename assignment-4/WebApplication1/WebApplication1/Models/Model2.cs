namespace WebApplication1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model2 : DbContext
    {
        public Model2()
            : base("name=packages")
        {
        }

        public virtual DbSet<Package> packages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Package>()
                .Property(e => e.sender)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.receiver)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.sender_city)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.destination_city)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.tracking);
        }
    }
}
