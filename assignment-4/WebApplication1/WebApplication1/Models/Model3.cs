namespace WebApplication1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model3 : DbContext
    {
        public Model3()
            : base("name=package_tracking")
        {
        }

        public virtual DbSet<PackageTracking> packageTracking { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PackageTracking>()
                .Property(e => e.package_id);

            modelBuilder.Entity<PackageTracking>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<PackageTracking>()
                .Property(e => e.time);
        }
    }
}
