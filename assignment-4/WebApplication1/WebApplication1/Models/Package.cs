namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tema4.packages")]
    public partial class Package
    {
        public int id { get; set; }

        [Required]
        [StringLength(45)]
        public string sender { get; set; }

        [Required]
        [StringLength(45)]
        public string receiver { get; set; }

        [Required]
        [StringLength(45)]
        public string name { get; set; }

        [Required]
        [StringLength(45)]
        public string description { get; set; }

        [Required]
        [StringLength(45)]
        public string sender_city { get; set; }

        [Required]
        [StringLength(45)]
        public string destination_city { get; set; }

        [Required]
        public bool tracking { get; set; }
    }
}
