package com.tema.model;

import java.io.Serializable;

public class Packages implements Serializable{
	
	private Integer id;
	private String sender;
	private String receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destinationCity;
	private boolean tracking;

	public Packages() {
	}

	public Packages(Integer id, String sender, String receiver, String name, String description, String senderCity,
			String destinationCity, boolean tracking) {
		this.id = id;
		this.sender = sender;
		this.receiver = receiver;
		this.name = name;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking = tracking;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSender() {
		return this.sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return this.receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return this.senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return this.destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public boolean isTracking() {
		return this.tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}



}
