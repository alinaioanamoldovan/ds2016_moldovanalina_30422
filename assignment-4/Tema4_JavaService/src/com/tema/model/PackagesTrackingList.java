package com.tema.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PackagesTrackingList implements Serializable{
	
	
	private ArrayList<PackagesTracking> packages;
	
	public PackagesTrackingList() {
		super();
	}

	public PackagesTrackingList(ArrayList<PackagesTracking> packages) {
		super();
		this.packages = packages;
	}

	

	public ArrayList<PackagesTracking> getPackages() {
		return packages;
	}

	public void setPackages(ArrayList<PackagesTracking> packages) {
		this.packages = packages;
	}
	
	

}
