package com.tema.model;

import java.io.Serializable;
import java.util.Date;

public class PackagesTracking implements Serializable {
	
	//private int id;
	private int package_id;
    private String city;
    private Date time;
    
	public PackagesTracking() {
		super();
	}
	public PackagesTracking(int package_id, String city, Date time) {
		super();
		//this.id = id;
		this.package_id = package_id;
		this.city = city;
		this.time = time;
	}
	
	public int getPackage_id() {
		return package_id;
	}
	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
    
    
    

}
