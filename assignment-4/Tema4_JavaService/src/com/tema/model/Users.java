package com.tema.model;

import java.io.Serializable;

public class Users implements Serializable{
	
	private String username;
	private String password;
	private boolean is_admin;
	
	
	public Users(){
		super();
	}
	
	public Users(String username,String password,boolean is_admin)
	{
		this.username = username;
		this.password = password;
		this.is_admin = is_admin;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
		
	}
	
	public String getUsername()
	{
		return this.username;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return this.password;
	}
	
	public void setIsAdmin(boolean admin)
	{
		this.is_admin = admin;
	}
	public boolean isAdmin()
	{
		return this.is_admin;
	}

}
