package com.tema.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PackagesList implements Serializable{
	
	private ArrayList<Packages> packages;
	
	public PackagesList() {
		super();
	}

	public PackagesList(ArrayList<Packages> packages) {
		super();
		this.packages = packages;
	}

	

	public ArrayList<Packages> getPackages() {
		return packages;
	}

	public void setPackages(ArrayList<Packages> packages) {
		this.packages = packages;
	}
	
	

}
