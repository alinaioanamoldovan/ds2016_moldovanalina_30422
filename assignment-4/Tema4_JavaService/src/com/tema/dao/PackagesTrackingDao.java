package com.tema.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import com.tema.model.PackagesTracking;

public class PackagesTrackingDao {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/tema4?useSSL=false";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "root";

	private Connection conn;

	public PackagesTrackingDao() {
		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<PackagesTracking> checkPackageStatus(int packageIdToCheck) {
		
		ArrayList<PackagesTracking> packageStatus = new ArrayList<PackagesTracking>();
		
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM package_tracking WHERE package_id = '" + packageIdToCheck + "'";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int packageId = rs.getInt("package_id");
				String city = rs.getString("city");
				Date time = rs.getDate("time");

				PackagesTracking packageTracking = new PackagesTracking(packageId, city, time);
				packageStatus.add(packageTracking);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packageStatus;
	}
	
}
