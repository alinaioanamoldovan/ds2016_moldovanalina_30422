package com.tema.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tema.model.Packages;

public class PackagesDao {
	
	// JDBC driver name and database URL
		static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		static final String DB_URL = "jdbc:mysql://localhost:3306/tema4?useSSL=false";

		// Database credentials
		static final String USER = "root";
		static final String PASS = "root";

		private Connection conn;

		public PackagesDao() {
			try {
				// Register JDBC driver
				Class.forName("com.mysql.jdbc.Driver");

				conn = DriverManager.getConnection(DB_URL, USER, PASS);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		
		public ArrayList<Packages> listClientPackages(String clientId) {
			
			ArrayList<Packages> packages = new ArrayList<Packages>();
			
			try {
				Statement stmt;
				stmt = conn.createStatement();
				String sql;
				sql = "SELECT * FROM packages WHERE sender = '" + clientId + "' OR receiver = '" + clientId + "'";
				ResultSet rs = stmt.executeQuery(sql);

				// Extract data from result set
				while (rs.next()) {
					// Retrieve by column name
					int id = rs.getInt("id");
					String sender = rs.getString("sender");
					String receiver = rs.getString("receiver");
					String name = rs.getString("name");
					String description = rs.getString("description");
					String senderCity = rs.getString("sender_city");
					String destinationCity = rs.getString("destination_city");
					boolean tracking = rs.getBoolean("tracking");


					Packages newPackage = new Packages(id, sender, receiver, 
							name, description, senderCity, destinationCity, tracking);
					packages.add(newPackage);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return packages;
		}
		
		public ArrayList<Packages> searchPackages(String packageName) {
			
			ArrayList<Packages> packages = new ArrayList<Packages>();
			
			try {
				Statement stmt;
				stmt = conn.createStatement();
				String sql;
				sql = "SELECT * FROM packages WHERE name LIKE '%" + packageName + "%'";
				ResultSet rs = stmt.executeQuery(sql);

				// Extract data from result set
				while (rs.next()) {
					// Retrieve by column name
					int id = rs.getInt("id");
					String sender = rs.getString("sender");
					String receiver = rs.getString("receiver");
					String name = rs.getString("name");
					String description = rs.getString("description");
					String senderCity = rs.getString("sender_city");
					String destinationCity = rs.getString("destination_city");
					boolean tracking = rs.getBoolean("tracking");


					Packages newPackage = new Packages(id, sender, receiver, name, description, senderCity, destinationCity, tracking);
					packages.add(newPackage);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return packages;
		}
		
}


