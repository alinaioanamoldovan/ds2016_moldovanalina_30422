package com.tema.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tema.model.Users;

public class UsersDao {
	// JDBC driver name and database URL
		static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		static final String DB_URL = "jdbc:mysql://localhost:3306/tema4?useSSL=false";

		// Database credentials
		static final String USER = "root";
		static final String PASS = "root";

		private Connection conn;

		public UsersDao() {
			try {
				// Register JDBC driver
				Class.forName("com.mysql.jdbc.Driver");

				conn = DriverManager.getConnection(DB_URL, USER, PASS);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		public Users login(String username, String password) {

			Users loggedInUser = null;

			try {
				Statement stmt;
				stmt = conn.createStatement();
				String sql;
				sql = "SELECT * FROM users WHERE username = '" + username + "' AND password = '" + password + "'";
				ResultSet rs = stmt.executeQuery(sql);

				// Extract data from result set
				while (rs.next()) {
					// Retrieve by column name
					String user = rs.getString("username");
					String pass = rs.getString("password");
					boolean isAdmin = rs.getBoolean("is_admin");

					loggedInUser = new Users(user, pass, isAdmin);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return loggedInUser;
		}

		public int register(String username, String password) {
			try {

				Statement stmt;
				stmt = conn.createStatement();
				String sql;
				sql = "INSERT INTO users VALUES ('" + username + "', '" + password + "', 0)";
                int res = stmt.executeUpdate(sql);
                return res;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return 0;
		}


}
