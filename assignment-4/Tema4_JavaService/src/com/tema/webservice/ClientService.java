package com.tema.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.tema.model.PackagesList;
import com.tema.model.PackagesTrackingList;


@WebService
@SOAPBinding(style = Style.RPC)
public interface ClientService {
	@WebMethod
	int login(String username, String password);
	
	@WebMethod
	int register(String username, String password);
	
	@WebMethod
	PackagesList listAllPackages(String username);
	
	@WebMethod
	PackagesList searchPackages(String name);
	
	@WebMethod
	PackagesTrackingList checkPackageStatus(int packageIdToCheck);

}
