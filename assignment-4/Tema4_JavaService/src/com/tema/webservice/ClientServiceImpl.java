package com.tema.webservice;

import javax.jws.WebService;

import com.tema.dao.PackagesDao;
import com.tema.dao.PackagesTrackingDao;
import com.tema.dao.UsersDao;
import com.tema.model.PackagesList;
import com.tema.model.PackagesTrackingList;
import com.tema.model.Users;

@WebService(endpointInterface = "com.tema.webservice.ClientService")
public class ClientServiceImpl implements ClientService {
	@Override
	public int login(String username, String password) {

		UsersDao usersDAO = new UsersDao();
		
		Users user = usersDAO.login(username, password);
		
		if (user != null) {
			
			if (password.equals(user.getPassword())) {
				return (user.isAdmin()) ? 1 : 0;
			} else {
				return -2; // wrong password
			}
			
		} else {
			return -1;	// user not found
		}
	}

	@Override
	public int register(String username, String password) {
		
		UsersDao usersDAO = new UsersDao();
		return usersDAO.register(username, password);
		
	}

	@Override
	public PackagesList listAllPackages(String username) {

		PackagesDao packagesDAO = new PackagesDao();
		
		return new PackagesList(packagesDAO.listClientPackages(username));
		
	}

	@Override
	public PackagesList searchPackages(String name) {
		PackagesDao packagesDAO = new PackagesDao();
		
		return new PackagesList(packagesDAO.searchPackages(name));
	}

	@Override
	public PackagesTrackingList checkPackageStatus(int packageIdToCheck) {
		PackagesTrackingDao packageTrackingDAO = new PackagesTrackingDao();
		
		return new PackagesTrackingList(packageTrackingDAO.checkPackageStatus(packageIdToCheck));
	}

}
