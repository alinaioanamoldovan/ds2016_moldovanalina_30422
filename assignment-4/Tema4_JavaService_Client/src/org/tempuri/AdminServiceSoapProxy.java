package org.tempuri;

public class AdminServiceSoapProxy implements org.tempuri.AdminServiceSoap {
  private String _endpoint = null;
  private org.tempuri.AdminServiceSoap adminServiceSoap = null;
  
  public AdminServiceSoapProxy() {
    _initAdminServiceSoapProxy();
  }
  
  public AdminServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initAdminServiceSoapProxy();
  }
  
  private void _initAdminServiceSoapProxy() {
    try {
      adminServiceSoap = (new org.tempuri.AdminServiceLocator()).getAdminServiceSoap();
      if (adminServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)adminServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)adminServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (adminServiceSoap != null)
      ((javax.xml.rpc.Stub)adminServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.AdminServiceSoap getAdminServiceSoap() {
    if (adminServiceSoap == null)
      _initAdminServiceSoapProxy();
    return adminServiceSoap;
  }
  
  public void addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String description, java.lang.String sender_city, java.lang.String destination_city) throws java.rmi.RemoteException{
    if (adminServiceSoap == null)
      _initAdminServiceSoapProxy();
    adminServiceSoap.addPackage(sender, receiver, name, description, sender_city, destination_city);
  }
  
  public void removePackage(int id) throws java.rmi.RemoteException{
    if (adminServiceSoap == null)
      _initAdminServiceSoapProxy();
    adminServiceSoap.removePackage(id);
  }
  
  public void registerPackageForTracking(int packageId) throws java.rmi.RemoteException{
    if (adminServiceSoap == null)
      _initAdminServiceSoapProxy();
    adminServiceSoap.registerPackageForTracking(packageId);
  }
  
  public void updatePackageStatus(int packageId, java.lang.String city) throws java.rmi.RemoteException{
    if (adminServiceSoap == null)
      _initAdminServiceSoapProxy();
    adminServiceSoap.updatePackageStatus(packageId, city);
  }
  
  
}