/**
 * AdminServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class AdminServiceLocator extends org.apache.axis.client.Service implements org.tempuri.AdminService {

    public AdminServiceLocator() {
    }


    public AdminServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AdminServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AdminServiceSoap
    private java.lang.String AdminServiceSoap_address = "http://localhost:64656/AdminService.asmx";

    public java.lang.String getAdminServiceSoapAddress() {
        return AdminServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AdminServiceSoapWSDDServiceName = "AdminServiceSoap";

    public java.lang.String getAdminServiceSoapWSDDServiceName() {
        return AdminServiceSoapWSDDServiceName;
    }

    public void setAdminServiceSoapWSDDServiceName(java.lang.String name) {
        AdminServiceSoapWSDDServiceName = name;
    }

    public org.tempuri.AdminServiceSoap getAdminServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AdminServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAdminServiceSoap(endpoint);
    }

    public org.tempuri.AdminServiceSoap getAdminServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.AdminServiceSoapStub _stub = new org.tempuri.AdminServiceSoapStub(portAddress, this);
            _stub.setPortName(getAdminServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAdminServiceSoapEndpointAddress(java.lang.String address) {
        AdminServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.AdminServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.AdminServiceSoapStub _stub = new org.tempuri.AdminServiceSoapStub(new java.net.URL(AdminServiceSoap_address), this);
                _stub.setPortName(getAdminServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AdminServiceSoap".equals(inputPortName)) {
            return getAdminServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "AdminService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "AdminServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AdminServiceSoap".equals(portName)) {
            setAdminServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
