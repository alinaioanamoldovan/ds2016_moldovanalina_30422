/**
 * AdminServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface AdminServiceSoap extends java.rmi.Remote {
    public void addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String description, java.lang.String sender_city, java.lang.String destination_city) throws java.rmi.RemoteException;
    public void removePackage(int id) throws java.rmi.RemoteException;
    public void registerPackageForTracking(int packageId) throws java.rmi.RemoteException;
    public void updatePackageStatus(int packageId, java.lang.String city) throws java.rmi.RemoteException;
}
