package com.tema.webservice;

public class ClientServiceProxy implements com.tema.webservice.ClientService {
  private String _endpoint = null;
  private com.tema.webservice.ClientService clientService = null;
  
  public ClientServiceProxy() {
    _initClientServiceProxy();
  }
  
  public ClientServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initClientServiceProxy();
  }
  
  private void _initClientServiceProxy() {
    try {
      clientService = (new com.tema.webservice.ClientServiceImplServiceLocator()).getClientServiceImplPort();
      if (clientService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)clientService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)clientService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (clientService != null)
      ((javax.xml.rpc.Stub)clientService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.tema.webservice.ClientService getClientService() {
    if (clientService == null)
      _initClientServiceProxy();
    return clientService;
  }
  
  public int register(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (clientService == null)
      _initClientServiceProxy();
    return clientService.register(arg0, arg1);
  }
  
  public com.tema.webservice.PackagesTracking[] checkPackageStatus(int arg0) throws java.rmi.RemoteException{
    if (clientService == null)
      _initClientServiceProxy();
    return clientService.checkPackageStatus(arg0);
  }
  
  public com.tema.webservice.Packages[] listAllPackages(java.lang.String arg0) throws java.rmi.RemoteException{
    if (clientService == null)
      _initClientServiceProxy();
    return clientService.listAllPackages(arg0);
  }
  
  public int login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (clientService == null)
      _initClientServiceProxy();
    return clientService.login(arg0, arg1);
  }
  
  public com.tema.webservice.Packages[] searchPackages(java.lang.String arg0) throws java.rmi.RemoteException{
    if (clientService == null)
      _initClientServiceProxy();
    return clientService.searchPackages(arg0);
  }
  
  
}