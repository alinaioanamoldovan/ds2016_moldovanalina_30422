/**
 * ClientService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tema.webservice;

public interface ClientService extends java.rmi.Remote {
    public int register(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public com.tema.webservice.PackagesTracking[] checkPackageStatus(int arg0) throws java.rmi.RemoteException;
    public com.tema.webservice.Packages[] listAllPackages(java.lang.String arg0) throws java.rmi.RemoteException;
    public int login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public com.tema.webservice.Packages[] searchPackages(java.lang.String arg0) throws java.rmi.RemoteException;
}
