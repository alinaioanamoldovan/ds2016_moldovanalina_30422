/**
 * ClientServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tema.webservice;

public class ClientServiceImplServiceLocator extends org.apache.axis.client.Service implements com.tema.webservice.ClientServiceImplService {

    public ClientServiceImplServiceLocator() {
    }


    public ClientServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ClientServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ClientServiceImplPort
    private java.lang.String ClientServiceImplPort_address = "http://localhost:9090/ws/ClientService";

    public java.lang.String getClientServiceImplPortAddress() {
        return ClientServiceImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ClientServiceImplPortWSDDServiceName = "ClientServiceImplPort";

    public java.lang.String getClientServiceImplPortWSDDServiceName() {
        return ClientServiceImplPortWSDDServiceName;
    }

    public void setClientServiceImplPortWSDDServiceName(java.lang.String name) {
        ClientServiceImplPortWSDDServiceName = name;
    }

    public com.tema.webservice.ClientService getClientServiceImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ClientServiceImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getClientServiceImplPort(endpoint);
    }

    public com.tema.webservice.ClientService getClientServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.tema.webservice.ClientServiceImplPortBindingStub _stub = new com.tema.webservice.ClientServiceImplPortBindingStub(portAddress, this);
            _stub.setPortName(getClientServiceImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setClientServiceImplPortEndpointAddress(java.lang.String address) {
        ClientServiceImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.tema.webservice.ClientService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.tema.webservice.ClientServiceImplPortBindingStub _stub = new com.tema.webservice.ClientServiceImplPortBindingStub(new java.net.URL(ClientServiceImplPort_address), this);
                _stub.setPortName(getClientServiceImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ClientServiceImplPort".equals(inputPortName)) {
            return getClientServiceImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.tema.com/", "ClientServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.tema.com/", "ClientServiceImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ClientServiceImplPort".equals(portName)) {
            setClientServiceImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
