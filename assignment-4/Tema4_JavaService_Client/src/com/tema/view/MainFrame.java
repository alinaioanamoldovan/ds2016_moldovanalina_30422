package com.tema.view;

import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.rpc.ServiceException;

import org.tempuri.AdminServiceLocator;
import org.tempuri.AdminServiceSoap;

import com.tema.webservice.ClientService;
import com.tema.webservice.ClientServiceImplServiceLocator;
import com.tema.webservice.Packages;
import com.tema.webservice.PackagesTracking;




public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField loginUsername;
	private JPasswordField loginPassword;
	private AdminServiceSoap adminService;
	private ClientService clientService;
	private JTextField senderText;
	private JTextField receiverText;
	private JTextField packageNameText;
	private JTextField descriptionText;
	private JTextField senderCityText;
	private JTextField destinationCityText;
	private JTextField packageIdRegisterText;
	private JTextField packageIdUpdateText;
	private JTextField cityUpdateText;
	private JLabel lblWelcome;
	private String currentUser;
	private JTextField packageNameSearchText;
	private JTable packagesTable;
	private JTextField checkPackageIdText;
	private JTable packageStatusTable;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() throws ServiceException, RemoteException {
		AdminServiceLocator adminServiceLocator = new AdminServiceLocator();
		adminService = adminServiceLocator.getAdminServiceSoap();

		ClientServiceImplServiceLocator clientServiceLocator = new ClientServiceImplServiceLocator();
		clientService = clientServiceLocator.getClientServiceImplPort();
		
		setTitle("Tracking System");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 714, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		JPanel loginPanel = new JPanel();
		contentPane.add(loginPanel, "loginPanel");
		
		loginPanel.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(200, 100, 150, 50);
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(200,150,150,50);
		loginUsername = new JTextField();
		loginUsername.setBounds(270,110,150,30);
		
		loginPassword = new JPasswordField();
		loginPassword.setBounds(270,160,150,30);
		JButton loginBtn = new JButton("Login");
		loginBtn.setBounds(200,220,70,30);
		
		JButton registerBtn = new JButton("Register");
		registerBtn.setBounds(350,220,100,30);
		
		JLabel lblLogin = new JLabel("Already have an account? Just login!");
		lblLogin.setBounds(220,280,250,30);
		
		JLabel lblRegister = new JLabel("You don't have an account? Just register!");
		lblRegister.setBounds(210, 300, 250, 30);
		loginPanel.add(lblUsername);
		loginPanel.add(lblPassword);
		loginPanel.add(loginUsername);
		loginPanel.add(loginPassword);
		loginPanel.add(loginBtn);
		loginPanel.add(registerBtn);
		loginPanel.add(lblLogin);
		loginPanel.add(lblRegister);
		
		loginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				@SuppressWarnings("deprecation")
				String password = loginPassword.getText();
				
				int loginStatus = -1;
				
				try {
					loginStatus = clientService.login(username, password);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				currentUser = username;
				if (loginStatus == 1) {
					lblWelcome.setText("Welcome, " + currentUser);
					((CardLayout)contentPane.getLayout()).show(contentPane, "adminPanel");
				}
				else if (loginStatus == 0) {
					// get packages
					try {
						Packages[] packages = clientService.listAllPackages(currentUser);
						fillPackagesTable(packages);
						
						for (int i = 0; i < packages.length; i++) {
							Packages packageVar = packages[i];
							
							
						}
						
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}

					label.setText("Welcome, " + currentUser);
					((CardLayout)contentPane.getLayout()).show(contentPane, "clientPanel");
				}
				
				loginUsername.setText("");
				loginPassword.setText("");
			}
		});
		
		registerBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				String password = loginPassword.getText();
				
				int status = 0;
				try {
					status = clientService.register(username, password);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				if (status > 0) {
					currentUser = username;
					((CardLayout)contentPane.getLayout()).show(contentPane, "clientPanel");
					loginUsername.setText("");
					loginPassword.setText("");
				}
			}
		});
		
		JPanel adminPanel = new JPanel();
		contentPane.add(adminPanel,"adminPanel");
		adminPanel.setLayout(null);
		lblWelcome = new JLabel("Welcome,");
		lblWelcome.setBounds(0, 0, 100, 30);
		JLabel lblAddPackage = new JLabel("Add package");
		lblAddPackage.setBounds(0, 20, 100, 50);
        JLabel lblSender = new JLabel("Sender");
		lblSender.setBounds(0, 60, 100, 50);
		senderText = new JTextField();
		senderText.setColumns(10);
		senderText.setBounds(90, 60, 100, 20);
        JLabel lblReceiver = new JLabel("Receiver");
		lblReceiver.setBounds(0, 90, 100, 30);
		receiverText = new JTextField();
		receiverText.setBounds(90,90,100,30);
        JLabel lblName = new JLabel("Name");
		lblName.setBounds(0, 120, 100, 50);
		packageNameText = new JTextField();
		packageNameText.setBounds(90, 120, 100, 30);
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(0, 150, 100, 50);
		descriptionText = new JTextField();
		descriptionText.setBounds(90,150,100,30);
		descriptionText.setColumns(10);
		JLabel lblSenderCity = new JLabel("Sender City");
		lblSenderCity.setBounds(0,180,100,50);
		senderCityText = new JTextField();
		senderCityText.setColumns(10);
		senderCityText.setBounds(90,180,100,30);
		JLabel lblDestinationCity = new JLabel("Destination City");
		lblDestinationCity.setBounds(0,210,100,30);
		destinationCityText = new JTextField();
		destinationCityText.setColumns(10);
		destinationCityText.setBounds(90,210,100,30);
		
		JButton btnAddPackage = new JButton("Submit");
		btnAddPackage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String sender = senderText.getText();
				String receiver = receiverText.getText();
				String packageName = packageNameText.getText();
				String description = descriptionText.getText();
				String sender_city = senderCityText.getText();
				String destination_city = destinationCityText.getText();
				
				try {
					adminService.addPackage(sender, receiver, packageName, description, sender_city, destination_city);
					
					JOptionPane.showMessageDialog(contentPane, "Package added!",destination_city, JOptionPane.WARNING_MESSAGE);
					
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				senderText.setText("");
				receiverText.setText("");
				packageNameText.setText("");
				descriptionText.setText("");
				senderCityText.setText("");
				destinationCityText.setText("");
				
			}
		});
		btnAddPackage.setBounds(0,250,100,30);
		
		JLabel lblRegisterPackageFor = new JLabel("Register package for tracking");
		lblRegisterPackageFor.setBounds(400, 30, 250, 70);
		JLabel lblPackageId = new JLabel("Package ID");
		lblPackageId.setBounds(400,60,100,50);
		packageIdRegisterText = new JTextField();
		packageIdRegisterText.setColumns(10);
		packageIdRegisterText.setBounds(400, 100, 100, 30);
		JButton btnRegisterPackage = new JButton("Register package");
		btnRegisterPackage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int packageId = Integer.parseInt(packageIdRegisterText.getText());
				
				try {
					adminService.registerPackageForTracking(packageId);
					JOptionPane.showMessageDialog(contentPane, "Package registered for tracking!",currentUser, JOptionPane.WARNING_MESSAGE);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				packageIdRegisterText.setText("");
			}
		});
		btnRegisterPackage.setBounds(400,140,150,30);
		JLabel lblUpdatePackageStatus = new JLabel("Update package status");
		lblUpdatePackageStatus.setBounds(400,180,150,30);
		JLabel lblPackageId_1 = new JLabel("Package ID");
		lblPackageId_1.setBounds(400, 220, 100, 30);
		packageIdUpdateText = new JTextField();
		packageIdUpdateText.setColumns(10);
		packageIdUpdateText.setBounds(400,260,100,30);
		JLabel lblCity = new JLabel("City");
		lblCity.setBounds(400,300,100,30);
		cityUpdateText = new JTextField();
		cityUpdateText.setColumns(10);
		cityUpdateText.setBounds(400,340,100,30);
		JButton btnUpdatePackage = new JButton("Update package");
		btnUpdatePackage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int packageId = Integer.parseInt(packageIdUpdateText.getText());
				String city = cityUpdateText.getText();
				
				try {
					adminService.updatePackageStatus(packageId, city);
					JOptionPane.showMessageDialog(contentPane, "Package status updated!",city, JOptionPane.WARNING_MESSAGE);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				
				packageIdUpdateText.setText("");
				cityUpdateText.setText("");
				
			}
		});
		JButton btnBack = new JButton("Logout");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentUser = "";
				((CardLayout)contentPane.getLayout()).show(contentPane, "loginPanel");
				
			}
		});
		btnBack.setBounds(580,0,100,30);
		btnUpdatePackage.setBounds(400, 380, 150, 30);
		adminPanel.add(btnBack);
		adminPanel.add(lblWelcome);
		adminPanel.add(lblAddPackage);
		adminPanel.add(lblSender);
		adminPanel.add(senderText);
		adminPanel.add(lblReceiver);
		adminPanel.add(receiverText);
		adminPanel.add(lblName);
		adminPanel.add(packageNameText);
		adminPanel.add(lblDescription);
		adminPanel.add(descriptionText);
		adminPanel.add(lblSenderCity);
		adminPanel.add(senderCityText);
		adminPanel.add(lblDestinationCity);
		adminPanel.add(destinationCityText);
		adminPanel.add(btnAddPackage);
		adminPanel.add(lblRegisterPackageFor);
		adminPanel.add(lblPackageId);
		adminPanel.add(packageIdRegisterText);
		adminPanel.add(btnRegisterPackage);
		adminPanel.add(lblUpdatePackageStatus);
		adminPanel.add(lblPackageId_1);
		adminPanel.add(packageIdUpdateText);
		adminPanel.add(lblCity);
		adminPanel.add(cityUpdateText);
		adminPanel.add(btnUpdatePackage);
		
		JPanel clientPanel = new JPanel();
		contentPane.add(clientPanel, "clientPanel");
		clientPanel.setLayout(null);
		//aici client
		JLabel lblClientPanel = new JLabel("Client Panel");
		lblClientPanel.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		label = new JLabel("Welcome,");
		label.setBounds(0,0,100,30);
		JLabel label2 = new JLabel("Search package");
		label2.setBounds(0,30,100,30);
		JButton btnBack_1 = new JButton("Logout");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentUser = "";
				checkPackageIdText.setText("");
				packageNameSearchText.setText("");
				
				fillPackagesTable(new Packages[0]);
				fillPackageStatusTable(new PackagesTracking[0]);
				
				((CardLayout)contentPane.getLayout()).show(contentPane, "loginPanel");
			}
		});
		btnBack_1.setBounds(580,0,100,30);
		JLabel lblPackageName = new JLabel("Package name");
		lblPackageName.setBounds(0, 70, 100, 30);
		packageNameSearchText = new JTextField();
		packageNameSearchText.setColumns(10);
		packageNameSearchText.setBounds(0,100,100,30);
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String packageName = packageNameSearchText.getText();
				try {
					Packages[] packages = clientService.searchPackages(packageName);
					fillPackagesTable(packages);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnSearch.setBounds(0,130,100,30);
		//packages
		packagesTable = new JTable();
		JScrollPane scrollPane = new JScrollPane(packagesTable);
		scrollPane.setBounds(0, 180, 650, 100);
		JLabel lblCheckPackageStatus = new JLabel("Check package status");
		lblCheckPackageStatus.setBounds(0, 280, 150, 50);
		JLabel lblPackageId_2 = new JLabel("Package ID");
		lblPackageId_2.setBounds(0,330,150,30);
		checkPackageIdText = new JTextField();
		checkPackageIdText.setColumns(10);
		checkPackageIdText.setBounds(0,370,100,30);
		JButton btnGo = new JButton("Go");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int packageID = Integer.parseInt(checkPackageIdText.getText());
				PackagesTracking[] packageStatus;
				try {
					packageStatus = clientService.checkPackageStatus(packageID);
					fillPackageStatusTable(packageStatus);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnGo.setBounds(0, 400, 100, 30);
		//status
		packageStatusTable = new JTable();
		JScrollPane scrollPane_1 = new JScrollPane(packageStatusTable);
		scrollPane_1.setBounds(0, 440, 650, 100);
		
		
		clientPanel.add(label);
		clientPanel.add(label2);
		clientPanel.add(btnBack_1);
		clientPanel.add(lblPackageName);
		clientPanel.add(packageNameSearchText);
		clientPanel.add(btnSearch);
		clientPanel.add(scrollPane);
		clientPanel.add(lblCheckPackageStatus);
		clientPanel.add(lblPackageId_2);
		clientPanel.add(checkPackageIdText);
		clientPanel.add(btnGo);
		clientPanel.add(scrollPane_1);
	}
	
	private void fillPackagesTable(Packages[] packages) {
		String col[] = {"ID", "Sender", "Receiver", "Name", "Description", "Sender City", "Destination City"};
		
		DefaultTableModel tm = new DefaultTableModel(col, 0);
		packagesTable.setModel(tm);
		
		for (int i = 0; i < packages.length; i++) {
			Packages pkg = packages[i];
			
			int id = pkg.getId();
			String sender = pkg.getSender();
			String receiver = pkg.getReceiver();
			String name = pkg.getName();
			String desc = pkg.getDescription();
			String senderCity = pkg.getSenderCity();
			String destinationCity = pkg.getDestinationCity();
			
			Object[] rowData = {id,sender,receiver,name,desc,senderCity,destinationCity};
			
			tm.addRow(rowData);
		}
	}
	
	private void fillPackageStatusTable(PackagesTracking[] packageStatus) {
		String col[] = {"Package ID", "City", "Time"};
		
		DefaultTableModel tm = new DefaultTableModel(col, 0);
		packageStatusTable.setModel(tm);
		
		for (int i = 0; i < packageStatus.length; i++) {
			PackagesTracking tracking = packageStatus[i];
			
			int packageId = tracking.getPackage_id();
			String city = tracking.getCity();
			String time = tracking.getTime().getTime().toString();
			
			Object[] rowData = {packageId,city,time};
			
			tm.addRow(rowData);
		}
	}
}
