<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleClientServiceProxyid" scope="session" class="com.tema.webservice.ClientServiceProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleClientServiceProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleClientServiceProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleClientServiceProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        com.tema.webservice.ClientService getClientService10mtemp = sampleClientServiceProxyid.getClientService();
if(getClientService10mtemp == null){
%>
<%=getClientService10mtemp %>
<%
}else{
        if(getClientService10mtemp!= null){
        String tempreturnp11 = getClientService10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String arg0_1id=  request.getParameter("arg016");
            java.lang.String arg0_1idTemp = null;
        if(!arg0_1id.equals("")){
         arg0_1idTemp  = arg0_1id;
        }
        String arg1_2id=  request.getParameter("arg118");
            java.lang.String arg1_2idTemp = null;
        if(!arg1_2id.equals("")){
         arg1_2idTemp  = arg1_2id;
        }
        int register13mtemp = sampleClientServiceProxyid.register(arg0_1idTemp,arg1_2idTemp);
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(register13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
break;
case 20:
        gotMethod = true;
        String arg0_3id=  request.getParameter("arg023");
        int arg0_3idTemp  = Integer.parseInt(arg0_3id);
        com.tema.webservice.PackagesTracking[] checkPackageStatus20mtemp = sampleClientServiceProxyid.checkPackageStatus(arg0_3idTemp);
if(checkPackageStatus20mtemp == null){
%>
<%=checkPackageStatus20mtemp %>
<%
}else{
        String tempreturnp21 = null;
        if(checkPackageStatus20mtemp != null){
        java.util.List listreturnp21= java.util.Arrays.asList(checkPackageStatus20mtemp);
        tempreturnp21 = listreturnp21.toString();
        }
        %>
        <%=tempreturnp21%>
        <%
}
break;
case 25:
        gotMethod = true;
        String arg0_4id=  request.getParameter("arg028");
            java.lang.String arg0_4idTemp = null;
        if(!arg0_4id.equals("")){
         arg0_4idTemp  = arg0_4id;
        }
        com.tema.webservice.Packages[] listAllPackages25mtemp = sampleClientServiceProxyid.listAllPackages(arg0_4idTemp);
if(listAllPackages25mtemp == null){
%>
<%=listAllPackages25mtemp %>
<%
}else{
        String tempreturnp26 = null;
        if(listAllPackages25mtemp != null){
        java.util.List listreturnp26= java.util.Arrays.asList(listAllPackages25mtemp);
        tempreturnp26 = listreturnp26.toString();
        }
        %>
        <%=tempreturnp26%>
        <%
}
break;
case 30:
        gotMethod = true;
        String arg0_5id=  request.getParameter("arg033");
            java.lang.String arg0_5idTemp = null;
        if(!arg0_5id.equals("")){
         arg0_5idTemp  = arg0_5id;
        }
        String arg1_6id=  request.getParameter("arg135");
            java.lang.String arg1_6idTemp = null;
        if(!arg1_6id.equals("")){
         arg1_6idTemp  = arg1_6id;
        }
        int login30mtemp = sampleClientServiceProxyid.login(arg0_5idTemp,arg1_6idTemp);
        String tempResultreturnp31 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(login30mtemp));
        %>
        <%= tempResultreturnp31 %>
        <%
break;
case 37:
        gotMethod = true;
        String arg0_7id=  request.getParameter("arg040");
            java.lang.String arg0_7idTemp = null;
        if(!arg0_7id.equals("")){
         arg0_7idTemp  = arg0_7id;
        }
        com.tema.webservice.Packages[] searchPackages37mtemp = sampleClientServiceProxyid.searchPackages(arg0_7idTemp);
if(searchPackages37mtemp == null){
%>
<%=searchPackages37mtemp %>
<%
}else{
        String tempreturnp38 = null;
        if(searchPackages37mtemp != null){
        java.util.List listreturnp38= java.util.Arrays.asList(searchPackages37mtemp);
        tempreturnp38 = listreturnp38.toString();
        }
        %>
        <%=tempreturnp38%>
        <%
}
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>