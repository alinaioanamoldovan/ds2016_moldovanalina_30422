<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleAdminServiceSoapProxyid" scope="session" class="org.tempuri.AdminServiceSoapProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleAdminServiceSoapProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleAdminServiceSoapProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleAdminServiceSoapProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        org.tempuri.AdminServiceSoap getAdminServiceSoap10mtemp = sampleAdminServiceSoapProxyid.getAdminServiceSoap();
if(getAdminServiceSoap10mtemp == null){
%>
<%=getAdminServiceSoap10mtemp %>
<%
}else{
        if(getAdminServiceSoap10mtemp!= null){
        String tempreturnp11 = getAdminServiceSoap10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String sender_1id=  request.getParameter("sender16");
            java.lang.String sender_1idTemp = null;
        if(!sender_1id.equals("")){
         sender_1idTemp  = sender_1id;
        }
        String receiver_2id=  request.getParameter("receiver18");
            java.lang.String receiver_2idTemp = null;
        if(!receiver_2id.equals("")){
         receiver_2idTemp  = receiver_2id;
        }
        String name_3id=  request.getParameter("name20");
            java.lang.String name_3idTemp = null;
        if(!name_3id.equals("")){
         name_3idTemp  = name_3id;
        }
        String description_4id=  request.getParameter("description22");
            java.lang.String description_4idTemp = null;
        if(!description_4id.equals("")){
         description_4idTemp  = description_4id;
        }
        String sender_city_5id=  request.getParameter("sender_city24");
            java.lang.String sender_city_5idTemp = null;
        if(!sender_city_5id.equals("")){
         sender_city_5idTemp  = sender_city_5id;
        }
        String destination_city_6id=  request.getParameter("destination_city26");
            java.lang.String destination_city_6idTemp = null;
        if(!destination_city_6id.equals("")){
         destination_city_6idTemp  = destination_city_6id;
        }
        sampleAdminServiceSoapProxyid.addPackage(sender_1idTemp,receiver_2idTemp,name_3idTemp,description_4idTemp,sender_city_5idTemp,destination_city_6idTemp);
break;
case 28:
        gotMethod = true;
        String id_7id=  request.getParameter("id31");
        int id_7idTemp  = Integer.parseInt(id_7id);
        sampleAdminServiceSoapProxyid.removePackage(id_7idTemp);
break;
case 33:
        gotMethod = true;
        String packageId_8id=  request.getParameter("packageId36");
        int packageId_8idTemp  = Integer.parseInt(packageId_8id);
        sampleAdminServiceSoapProxyid.registerPackageForTracking(packageId_8idTemp);
break;
case 38:
        gotMethod = true;
        String packageId_9id=  request.getParameter("packageId41");
        int packageId_9idTemp  = Integer.parseInt(packageId_9id);
        String city_10id=  request.getParameter("city43");
            java.lang.String city_10idTemp = null;
        if(!city_10id.equals("")){
         city_10idTemp  = city_10id;
        }
        sampleAdminServiceSoapProxyid.updatePackageStatus(packageId_9idTemp,city_10idTemp);
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>