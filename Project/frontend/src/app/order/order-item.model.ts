import { Product } from "../product/product.model";

export class OrderItem {
    constructor(public ordernumber: number, public quantity: number) {}
}