import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';
import { ShoppingCartService } from '../shopping-cart/shopping-cart.service';

import { Product } from './product.model';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: []
})
export class ProductListComponent implements OnInit {
  public products: Product[];
  errorMessage: string;

  constructor(private productService: ProductService, private cartService: ShoppingCartService) { }

  ngOnInit() {
    this.getProducts();
  }

  // TODO input validation
  addToCart(product: Product, quantity: string) {
    console.log(product);
    console.log(quantity);
    this.cartService.addToCart(product, parseInt(quantity));
  }

  getProducts() {
    this.productService.getProducts().subscribe(products => this.products = products, error => this.errorMessage = <any>error);
  }

}
