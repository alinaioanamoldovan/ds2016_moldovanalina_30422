webpackJsonp([0,3],{

/***/ 161:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderService = (function () {
    function OrderService(ahttp) {
        this.ahttp = ahttp;
    }
    OrderService.prototype.order = function (o) {
        console.log(o);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.ahttp.post('http://localhost:8080/api/order', o, options).toPromise()
            .then(function (r) {
            console.log(r);
        }).catch(function (r) {
            console.log(r);
            Promise.reject(r);
        });
    };
    OrderService.prototype.getOrders = function () {
        return this.ahttp.get('http://localhost:8080/api/order').toPromise()
            .then(function (r) {
            console.log(r);
            console.log(r.json());
            return r.json();
        }).catch(function (r) {
            console.log("error: " + r);
        });
    };
    OrderService.prototype.getOrder = function (id) {
        return this.ahttp.get('http://localhost:8080/api/order/' + id).toPromise().then(function (r) { return r.json(); }).catch(function (r) { return console.log(r); });
    };
    return OrderService;
}());
OrderService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angular2_jwt__["AuthHttp"]) === "function" && _a || Object])
], OrderService);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order.service.js.map

/***/ },

/***/ 162:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cart_item_model__ = __webpack_require__(724);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ShoppingCartService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ShoppingCartService = (function () {
    function ShoppingCartService() {
        this.dataStore = { items: [] };
        this._orders = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](this.dataStore.items);
    }
    Object.defineProperty(ShoppingCartService.prototype, "orders", {
        get: function () {
            console.log("getOrders");
            return this._orders.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ShoppingCartService.prototype, "total", {
        get: function () {
            var t = 0;
            this.dataStore.items.forEach(function (o, i) {
                t += o.product.price * o.quantity;
            });
            return t;
        },
        enumerable: true,
        configurable: true
    });
    ShoppingCartService.prototype.addToCart = function (product, quantity) {
        // merge orders for same pizzas, add quantity
        var found = false;
        for (var _i = 0, _a = this.dataStore.items; _i < _a.length; _i++) {
            var o = _a[_i];
            if (o.product.name == product.name) {
                var p = o.quantity + quantity;
                o.quantity = p;
                found = true;
            }
        }
        if (!found)
            this.dataStore.items.push(new __WEBPACK_IMPORTED_MODULE_2__cart_item_model__["a" /* CartItem */](product, quantity));
        this._orders.next(Object.assign({}, this.dataStore).items);
    };
    ShoppingCartService.prototype.delete = function (order) {
        var _this = this;
        console.log(order);
        this.dataStore.items.forEach(function (o, i) {
            if (o.product.name == order.product.name) {
                _this.dataStore.items.splice(i, 1);
            }
        });
        this._orders.next(Object.assign({}, this.dataStore).items);
    };
    ShoppingCartService.prototype.clean = function () {
        this.dataStore.items = [];
        this._orders.next(Object.assign({}, this.dataStore).items);
    };
    return ShoppingCartService;
}());
ShoppingCartService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], ShoppingCartService);

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/shopping-cart.service.js.map

/***/ },

/***/ 236:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__customer_model__ = __webpack_require__(714);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return CustomerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomerComponent = (function () {
    function CustomerComponent() {
    }
    CustomerComponent.prototype.ngOnInit = function () {
        this.customer = new __WEBPACK_IMPORTED_MODULE_1__customer_model__["a" /* Customer */]();
    };
    return CustomerComponent;
}());
CustomerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-customer',
        template: __webpack_require__(914),
        styles: [__webpack_require__(903)]
    }),
    __metadata("design:paramtypes", [])
], CustomerComponent);

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/customer.component.js.map

/***/ },

/***/ 237:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angular2_jwt__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return CustomerService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomerService = (function () {
    function CustomerService(ahttp) {
        this.ahttp = ahttp;
    }
    CustomerService.prototype.saveCustomer = function (customer) {
        return this.ahttp.post('http://localhost:8080/api/customer', JSON.stringify(customer)).toPromise();
    };
    CustomerService.prototype.getCustomer = function () {
        return this.ahttp.get("http://localhost:8080/api/customer").toPromise().then(function (c) {
            console.log(c);
            return c.json();
        }).catch(function (c) {
            console.log("no customer data: " + c);
            return Promise.reject("no customer data");
        });
    };
    return CustomerService;
}());
CustomerService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angular2_jwt__["AuthHttp"]) === "function" && _a || Object])
], CustomerService);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/customer.service.js.map

/***/ },

/***/ 371:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return RegisterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegisterComponent = (function () {
    function RegisterComponent() {
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-register',
        template: __webpack_require__(913),
        styles: [__webpack_require__(902)]
    }),
    __metadata("design:paramtypes", [])
], RegisterComponent);

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/register.component.js.map

/***/ },

/***/ 372:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(929);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__(928);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ProductService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProductService = (function () {
    function ProductService(http) {
        this.http = http;
    }
    ProductService.prototype.getProducts = function () {
        return this.http.get('http://localhost:8080/api/products')
            .map(this.getData)
            .catch(this.handleError);
    };
    ProductService.prototype.getData = function (res) {
        var body = res.json();
        console.log(body);
        return body;
    };
    ProductService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Response"]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(errMsg);
    };
    return ProductService;
}());
ProductService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]) === "function" && _a || Object])
], ProductService);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/product.service.js.map

/***/ },

/***/ 592:
/***/ function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 592;


/***/ },

/***/ 593:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__polyfills_ts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(678);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(709);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/main.js.map

/***/ },

/***/ 708:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__authentication_authentication_service__ = __webpack_require__(84);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(authService) {
        this.authService = authService;
        this.username = "";
    }
    AppComponent.prototype.logout = function () {
        this.authService.logout();
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.loggedIn$.subscribe(function (username) {
            console.log(username);
            _this.username = username;
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AppComponent.prototype.loggedIn = function () {
        return this.authService.isLoggedIn();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(911),
        styles: [__webpack_require__(900)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__authentication_authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__authentication_authentication_service__["a" /* AuthenticationService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/app.component.js.map

/***/ },

/***/ 709:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_jwt__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(708);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__product_product_detail_component__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__product_product_list_component__ = __webpack_require__(723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shopping_cart_shopping_cart_component__ = __webpack_require__(725);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__order_order_component__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__customer_customer_component__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__authentication_login_login_component__ = __webpack_require__(711);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__authentication_register_register_component__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__order_order_list_component__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__customer_edit_customer_component__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__order_order_detail_component__ = __webpack_require__(716);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__product_product_service__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__shopping_cart_shopping_cart_service__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__authentication_authentication_service__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__customer_customer_service__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__order_order_service__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__authentication_user_guard_service__ = __webpack_require__(712);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__authentication_admin_guard_service__ = __webpack_require__(710);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__order_order_details_resolver__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_ng2_bootstrap_tabs__ = __webpack_require__(559);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_ng2_bootstrap_modal__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27_ng2_bootstrap_dropdown__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28_ng2_bootstrap_alert__ = __webpack_require__(522);
/* unused harmony export authHttpServiceFactory */
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





























var appRoutes = [
    { path: 'product/:id', component: __WEBPACK_IMPORTED_MODULE_7__product_product_detail_component__["a" /* ProductDetailComponent */] },
    { path: 'product', component: __WEBPACK_IMPORTED_MODULE_8__product_product_list_component__["a" /* ProductListComponent */] },
    { path: 'order', component: __WEBPACK_IMPORTED_MODULE_10__order_order_component__["a" /* OrderComponent */] },
    { path: 'user/orders', component: __WEBPACK_IMPORTED_MODULE_14__order_order_list_component__["a" /* OrderListComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_22__authentication_user_guard_service__["a" /* UserGuard */]] },
    { path: 'user/order/:id', component: __WEBPACK_IMPORTED_MODULE_16__order_order_detail_component__["a" /* OrderDetailComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_22__authentication_user_guard_service__["a" /* UserGuard */]], resolve: { order: __WEBPACK_IMPORTED_MODULE_24__order_order_details_resolver__["a" /* OrderDetailsResolver */] } },
    { path: 'user/customer', component: __WEBPACK_IMPORTED_MODULE_15__customer_edit_customer_component__["a" /* EditCustomerComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_22__authentication_user_guard_service__["a" /* UserGuard */]] },
    { path: '', redirectTo: '/product', pathMatch: 'full' }
];
function authHttpServiceFactory(http, options) {
    return new __WEBPACK_IMPORTED_MODULE_5_angular2_jwt__["AuthHttp"](new __WEBPACK_IMPORTED_MODULE_5_angular2_jwt__["AuthConfig"]({
        noJwtError: true,
        globalHeaders: [{ 'Content-Type': 'application/json' }]
    }), http, options);
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_7__product_product_detail_component__["a" /* ProductDetailComponent */],
            __WEBPACK_IMPORTED_MODULE_8__product_product_list_component__["a" /* ProductListComponent */],
            __WEBPACK_IMPORTED_MODULE_9__shopping_cart_shopping_cart_component__["a" /* ShoppingCartComponent */],
            __WEBPACK_IMPORTED_MODULE_10__order_order_component__["a" /* OrderComponent */],
            __WEBPACK_IMPORTED_MODULE_11__customer_customer_component__["a" /* CustomerComponent */],
            __WEBPACK_IMPORTED_MODULE_12__authentication_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_13__authentication_register_register_component__["a" /* RegisterComponent */],
            __WEBPACK_IMPORTED_MODULE_14__order_order_list_component__["a" /* OrderListComponent */],
            __WEBPACK_IMPORTED_MODULE_15__customer_edit_customer_component__["a" /* EditCustomerComponent */],
            __WEBPACK_IMPORTED_MODULE_16__order_order_detail_component__["a" /* OrderDetailComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["b" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["HttpModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["JsonpModule"],
            __WEBPACK_IMPORTED_MODULE_25_ng2_bootstrap_tabs__["a" /* TabsModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_26_ng2_bootstrap_modal__["b" /* ModalModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_27_ng2_bootstrap_dropdown__["a" /* DropdownModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_28_ng2_bootstrap_alert__["a" /* AlertModule */].forRoot()
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_17__product_product_service__["a" /* ProductService */], __WEBPACK_IMPORTED_MODULE_18__shopping_cart_shopping_cart_service__["a" /* ShoppingCartService */], __WEBPACK_IMPORTED_MODULE_19__authentication_authentication_service__["a" /* AuthenticationService */], __WEBPACK_IMPORTED_MODULE_20__customer_customer_service__["a" /* CustomerService */],
            {
                provide: __WEBPACK_IMPORTED_MODULE_5_angular2_jwt__["AuthHttp"],
                useFactory: authHttpServiceFactory,
                deps: [__WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_3__angular_http__["RequestOptions"]]
            }, __WEBPACK_IMPORTED_MODULE_22__authentication_user_guard_service__["a" /* UserGuard */], __WEBPACK_IMPORTED_MODULE_23__authentication_admin_guard_service__["a" /* AdminGuard */], __WEBPACK_IMPORTED_MODULE_21__order_order_service__["a" /* OrderService */], __WEBPACK_IMPORTED_MODULE_24__order_order_details_resolver__["a" /* OrderDetailsResolver */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/app.module.js.map

/***/ },

/***/ 710:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_service__ = __webpack_require__(84);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AdminGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminGuard = (function () {
    function AdminGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AdminGuard.prototype.canActivate = function () {
        if (this.authService.isAdmin()) {
            return true;
        }
        this.router.navigate(['']);
        return false;
    };
    return AdminGuard;
}());
AdminGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AdminGuard);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/admin-guard.service.js.map

/***/ },

/***/ 711:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(885);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_service__ = __webpack_require__(84);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(authService) {
        this.authService = authService;
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        console.log(this.email + " " + this.pw);
        this.authService.login(this.email, this.pw).then(function () {
            _this.hideLogin();
        }).catch(function () { return _this.lerror = true; });
    };
    LoginComponent.prototype.showLogin = function () {
        this.loginModal.show();
    };
    LoginComponent.prototype.hideLogin = function () {
        this.loginModal.hide();
        this.email = "";
        this.pw = "";
        this.lerror = false;
    };
    return LoginComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('loginModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["a" /* ModalDirective */]) === "function" && _a || Object)
], LoginComponent.prototype, "loginModal", void 0);
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(912),
        styles: [__webpack_require__(901)],
        exportAs: 'child'
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/login.component.js.map

/***/ },

/***/ 712:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_service__ = __webpack_require__(84);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return UserGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserGuard = (function () {
    function UserGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    UserGuard.prototype.canActivate = function () {
        if (this.authService.isLoggedIn()) {
            return true;
        }
        this.router.navigate(['']);
        return false;
    };
    return UserGuard;
}());
UserGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], UserGuard);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/user-guard.service.js.map

/***/ },

/***/ 713:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    return User;
}());

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/user.model.js.map

/***/ },

/***/ 714:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return Customer; });
var Customer = (function () {
    function Customer() {
    }
    return Customer;
}());

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/customer.model.js.map

/***/ },

/***/ 715:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__customer_component__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__customer_service__ = __webpack_require__(237);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return EditCustomerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditCustomerComponent = (function () {
    function EditCustomerComponent(customerService) {
        this.customerService = customerService;
        this.saved = false;
        this.error = false;
        this.nocustomer = false;
    }
    EditCustomerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.customerService.getCustomer().then(function (c) {
            console.log(c);
            _this.customerComponent.customer = c;
        }).catch(function (c) {
            console.log("no customer data: " + c);
            _this.nocustomer = true;
        });
    };
    EditCustomerComponent.prototype.save = function () {
        var _this = this;
        var c = this.customerComponent.customer;
        this.customerService.saveCustomer(c).then(function () {
            _this.nocustomer = false;
            _this.saved = true;
        }).catch(function () {
            _this.error = true;
            _this.saved = false;
            _this.nocustomer = false;
        });
    };
    return EditCustomerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__customer_component__["a" /* CustomerComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__customer_component__["a" /* CustomerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__customer_component__["a" /* CustomerComponent */]) === "function" && _a || Object)
], EditCustomerComponent.prototype, "customerComponent", void 0);
EditCustomerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-edit-customer',
        template: __webpack_require__(915),
        styles: [__webpack_require__(904)]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__customer_service__["a" /* CustomerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__customer_service__["a" /* CustomerService */]) === "function" && _b || Object])
], EditCustomerComponent);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/edit-customer.component.js.map

/***/ },

/***/ 716:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(60);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderDetailComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderDetailComponent = (function () {
    function OrderDetailComponent(route) {
        this.route = route;
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.order = data.order;
        });
    };
    return OrderDetailComponent;
}());
OrderDetailComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order-detail',
        template: __webpack_require__(916),
        styles: [__webpack_require__(905)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object])
], OrderDetailComponent);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order-detail.component.js.map

/***/ },

/***/ 717:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_service__ = __webpack_require__(161);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderDetailsResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderDetailsResolver = (function () {
    function OrderDetailsResolver(orderService, router) {
        this.orderService = orderService;
        this.router = router;
    }
    OrderDetailsResolver.prototype.resolve = function (route, state) {
        var _this = this;
        var id = route.params['id'];
        return this.orderService.getOrder(id).then(function (order) { return order; }).catch(function () {
            _this.router.navigate(['/user/orders']);
            return null;
        });
    };
    return OrderDetailsResolver;
}());
OrderDetailsResolver = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__order_service__["a" /* OrderService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], OrderDetailsResolver);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order-details-resolver.js.map

/***/ },

/***/ 718:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderItem; });
var OrderItem = (function () {
    function OrderItem(ordernumber, quantity) {
        this.ordernumber = ordernumber;
        this.quantity = quantity;
    }
    return OrderItem;
}());

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order-item.model.js.map

/***/ },

/***/ 719:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__order_service__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(60);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderListComponent = (function () {
    function OrderListComponent(orderService, router) {
        var _this = this;
        this.orderService = orderService;
        this.router = router;
        this.orders = [];
        this.noorders = false;
        this.error = false;
        this.orderService.getOrders()
            .then(function (o) {
            _this.orders = o;
            if (_this.orders.length == 0) {
                _this.noorders = true;
            }
        }).catch(function () { return _this.error = true; });
    }
    OrderListComponent.prototype.ngOnInit = function () {
    };
    OrderListComponent.prototype.showOrder = function (o) {
        console.log(o);
        this.router.navigate(['/user/order', o.id]);
    };
    return OrderListComponent;
}());
OrderListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order-list',
        template: __webpack_require__(917),
        styles: [__webpack_require__(906)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _b || Object])
], OrderListComponent);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order-list.component.js.map

/***/ },

/***/ 720:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_service__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__authentication_authentication_service__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__customer_customer_service__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__authentication_register_register_component__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__customer_customer_component__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__order_item_model__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__authentication_user_model__ = __webpack_require__(713);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__order_model__ = __webpack_require__(721);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return OrderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var OrderComponent = (function () {
    function OrderComponent(cartService, router, orderService, authService, customerService) {
        var _this = this;
        this.cartService = cartService;
        this.router = router;
        this.orderService = orderService;
        this.authService = authService;
        this.customerService = customerService;
        this.register = false;
        this.done = false;
        this.error = false;
        this.tabs = [
            {},
            { disabled: true }
        ];
        cartService.orders.subscribe(function (neworders) {
            _this.orders = neworders;
        });
    }
    /*
    if register -> register then
    create customer then
    create order
    */
    OrderComponent.prototype.orderNow = function () {
        var _this = this;
        if (this.register) {
            console.log("register == true");
            var u = new __WEBPACK_IMPORTED_MODULE_9__authentication_user_model__["a" /* User */]();
            u.mail = this.registerComponent.mail;
            // TODO pw
            u.password = this.registerComponent.pw1;
            this.authService.register(u).then(function (user) {
                _this.authService.login(user.mail, user.password).then(function (user) {
                });
                var o = new __WEBPACK_IMPORTED_MODULE_10__order_model__["a" /* Order */]();
                o.customer = _this.customerComponent.customer;
                o.customer.user = user;
                console.log(user);
                _this.orders.forEach(function (e) {
                    o.items.push(new __WEBPACK_IMPORTED_MODULE_8__order_item_model__["a" /* OrderItem */](e.product.ordernumber, e.quantity));
                });
                console.log(o);
                _this.orderService.order(o).then(function () { return _this.finished(); }).catch(function () { return _this.onerror(); });
            }).catch(function (e) {
                console.log(e);
                _this.registerComponent.msgexists = "E-Mail already registerd!";
            });
        }
        else {
            var o_1 = new __WEBPACK_IMPORTED_MODULE_10__order_model__["a" /* Order */]();
            o_1.customer = this.customerComponent.customer;
            this.orders.forEach(function (e) {
                o_1.items.push(new __WEBPACK_IMPORTED_MODULE_8__order_item_model__["a" /* OrderItem */](e.product.ordernumber, e.quantity));
            });
            console.log(o_1);
            this.orderService.order(o_1).then(function () { return _this.finished(); }).catch(function () { return _this.onerror(); });
        }
    };
    OrderComponent.prototype.finished = function () {
        this.done = true;
        this.cartService.clean();
    };
    OrderComponent.prototype.onerror = function () {
        this.error = true;
    };
    Object.defineProperty(OrderComponent.prototype, "total", {
        get: function () {
            return this.cartService.total;
        },
        enumerable: true,
        configurable: true
    });
    OrderComponent.prototype.switchTabs = function () {
        this.tabs[1].disabled = false;
        this.tabs[0].active = !this.tabs[0].active;
        this.tabs[1].active = !this.tabs[1].active;
        console.log(this.tabs);
    };
    // redirect, if there are no orders
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('init order component');
        if (this.orders.length == 0) {
            this.router.navigate(["/"]);
        }
        this.tabs[0].active = true;
        this.customerService.getCustomer().then(function (c) {
            console.log(c);
            _this.customerComponent.customer = c;
        }).catch(function () { return console.log(""); });
    };
    OrderComponent.prototype.loggedIn = function () {
        return this.authService.isLoggedIn();
    };
    return OrderComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6__authentication_register_register_component__["a" /* RegisterComponent */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6__authentication_register_register_component__["a" /* RegisterComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__authentication_register_register_component__["a" /* RegisterComponent */]) === "function" && _a || Object)
], OrderComponent.prototype, "registerComponent", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__customer_customer_component__["a" /* CustomerComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_7__customer_customer_component__["a" /* CustomerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__customer_customer_component__["a" /* CustomerComponent */]) === "function" && _b || Object)
], OrderComponent.prototype, "customerComponent", void 0);
OrderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order',
        template: __webpack_require__(918),
        styles: [__webpack_require__(907)],
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__["a" /* ShoppingCartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__["a" /* ShoppingCartService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__authentication_authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__authentication_authentication_service__["a" /* AuthenticationService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__customer_customer_service__["a" /* CustomerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__customer_customer_service__["a" /* CustomerService */]) === "function" && _g || Object])
], OrderComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order.component.js.map

/***/ },

/***/ 721:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return Order; });
var Order = (function () {
    function Order() {
        this.items = [];
    }
    return Order;
}());

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/order.model.js.map

/***/ },

/***/ 722:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ProductDetailComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductDetailComponent = (function () {
    function ProductDetailComponent() {
    }
    ProductDetailComponent.prototype.ngOnInit = function () {
    };
    return ProductDetailComponent;
}());
ProductDetailComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-product-detail',
        template: __webpack_require__(919),
        styles: [__webpack_require__(908)]
    }),
    __metadata("design:paramtypes", [])
], ProductDetailComponent);

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/product-detail.component.js.map

/***/ },

/***/ 723:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__product_service__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__ = __webpack_require__(162);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ProductListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductListComponent = (function () {
    function ProductListComponent(productService, cartService) {
        this.productService = productService;
        this.cartService = cartService;
    }
    ProductListComponent.prototype.ngOnInit = function () {
        this.getProducts();
    };
    // TODO input validation
    ProductListComponent.prototype.addToCart = function (product, quantity) {
        console.log(product);
        console.log(quantity);
        this.cartService.addToCart(product, parseInt(quantity));
    };
    ProductListComponent.prototype.getProducts = function () {
        var _this = this;
        this.productService.getProducts().subscribe(function (products) { return _this.products = products; }, function (error) { return _this.errorMessage = error; });
    };
    return ProductListComponent;
}());
ProductListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'product-list',
        template: __webpack_require__(920),
        styles: [__webpack_require__(909)],
        providers: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__product_service__["a" /* ProductService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__product_service__["a" /* ProductService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__["a" /* ShoppingCartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shopping_cart_shopping_cart_service__["a" /* ShoppingCartService */]) === "function" && _b || Object])
], ProductListComponent);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/product-list.component.js.map

/***/ },

/***/ 724:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return CartItem; });
var CartItem = (function () {
    function CartItem(product, quantity) {
        this.product = product;
        this.quantity = quantity;
    }
    return CartItem;
}());

//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/cart-item.model.js.map

/***/ },

/***/ 725:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shopping_cart_service__ = __webpack_require__(162);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return ShoppingCartComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShoppingCartComponent = (function () {
    function ShoppingCartComponent(cartService) {
        var _this = this;
        this.cartService = cartService;
        //this.orders = cartService.getOrders();
        cartService.orders.subscribe(function (items) {
            _this.items = items;
        });
    }
    ShoppingCartComponent.prototype.delete = function (o) {
        console.log(o);
        this.cartService.delete(o);
    };
    Object.defineProperty(ShoppingCartComponent.prototype, "total", {
        get: function () {
            return this.cartService.total;
        },
        enumerable: true,
        configurable: true
    });
    return ShoppingCartComponent;
}());
ShoppingCartComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'shopping-cart',
        template: __webpack_require__(921),
        styles: [__webpack_require__(910)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shopping_cart_service__["a" /* ShoppingCartService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shopping_cart_service__["a" /* ShoppingCartService */]) === "function" && _a || Object])
], ShoppingCartComponent);

var _a;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/shopping-cart.component.js.map

/***/ },

/***/ 726:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/environment.js.map

/***/ },

/***/ 727:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(734);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(740);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(729);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(728);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(731);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(953);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
// This file includes polyfills needed by Angular 2 and is loaded before
// the app. You can add your own extra polyfills to this file.
















//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/polyfills.js.map

/***/ },

/***/ 84:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(933);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AuthenticationService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthenticationService = (function () {
    function AuthenticationService(ahttp, router) {
        this.ahttp = ahttp;
        this.router = router;
        this.jsonheaders = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        this.jsonoptions = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: this.jsonheaders });
        this.loggedIn = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"]("");
        this.loggedIn$ = this.loggedIn.asObservable();
        this.jwtHelper = new __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["JwtHelper"]();
        if (localStorage.getItem("id_token") != null) {
            this.setUsername();
        }
    }
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        console.log(JSON.stringify({ username: username, password: password }));
        return this.ahttp.post("http://localhost:8080/api/login", JSON.stringify({ username: username, password: password }), this.jsonoptions)
            .toPromise().then(function (u) {
            var t = u.headers.get("authorization");
            t = t.replace("Bearer ", "");
            localStorage.setItem('id_token', t);
            _this.setUsername();
        }).catch(this.loginError);
    };
    AuthenticationService.prototype.setUsername = function () {
        var t = localStorage.getItem("id_token");
        console.log(this.jwtHelper.decodeToken(t));
        this.loggedIn.next(this.jwtHelper.decodeToken(t).sub);
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem('id_token');
        this.loggedIn.next("");
        this.router.navigate(['']);
    };
    AuthenticationService.prototype.register = function (reguser) {
        return this.ahttp.post('http://localhost:8080/api/user', JSON.stringify(reguser), this.jsonoptions).toPromise().then(function () { return reguser; }).catch(this.registerError);
    };
    AuthenticationService.prototype.isLoggedIn = function () {
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["tokenNotExpired"])();
    };
    AuthenticationService.prototype.isAdmin = function () {
        if (!this.isLoggedIn())
            return false;
        var t = localStorage.getItem("id_token");
        return this.jwtHelper.decodeToken(t).admin;
    };
    AuthenticationService.prototype.registerError = function (error) {
        console.log("register error");
        return Promise.reject(error.message || error);
    };
    AuthenticationService.prototype.loginError = function (error) {
        console.log("login failed");
        return Promise.reject(error.message || error);
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["AuthHttp"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular2_jwt__["AuthHttp"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AuthenticationService);

var _a, _b;
//# sourceMappingURL=C:/Users/Toshiba/Desktop/frontend/src/authentication.service.js.map

/***/ },

/***/ 879:
/***/ function(module, exports, __webpack_require__) {

var map = {
	"./af": 410,
	"./af.js": 410,
	"./ar": 416,
	"./ar-dz": 411,
	"./ar-dz.js": 411,
	"./ar-ly": 412,
	"./ar-ly.js": 412,
	"./ar-ma": 413,
	"./ar-ma.js": 413,
	"./ar-sa": 414,
	"./ar-sa.js": 414,
	"./ar-tn": 415,
	"./ar-tn.js": 415,
	"./ar.js": 416,
	"./az": 417,
	"./az.js": 417,
	"./be": 418,
	"./be.js": 418,
	"./bg": 419,
	"./bg.js": 419,
	"./bn": 420,
	"./bn.js": 420,
	"./bo": 421,
	"./bo.js": 421,
	"./br": 422,
	"./br.js": 422,
	"./bs": 423,
	"./bs.js": 423,
	"./ca": 424,
	"./ca.js": 424,
	"./cs": 425,
	"./cs.js": 425,
	"./cv": 426,
	"./cv.js": 426,
	"./cy": 427,
	"./cy.js": 427,
	"./da": 428,
	"./da.js": 428,
	"./de": 430,
	"./de-at": 429,
	"./de-at.js": 429,
	"./de.js": 430,
	"./dv": 431,
	"./dv.js": 431,
	"./el": 432,
	"./el.js": 432,
	"./en-au": 433,
	"./en-au.js": 433,
	"./en-ca": 434,
	"./en-ca.js": 434,
	"./en-gb": 435,
	"./en-gb.js": 435,
	"./en-ie": 436,
	"./en-ie.js": 436,
	"./en-nz": 437,
	"./en-nz.js": 437,
	"./eo": 438,
	"./eo.js": 438,
	"./es": 440,
	"./es-do": 439,
	"./es-do.js": 439,
	"./es.js": 440,
	"./et": 441,
	"./et.js": 441,
	"./eu": 442,
	"./eu.js": 442,
	"./fa": 443,
	"./fa.js": 443,
	"./fi": 444,
	"./fi.js": 444,
	"./fo": 445,
	"./fo.js": 445,
	"./fr": 448,
	"./fr-ca": 446,
	"./fr-ca.js": 446,
	"./fr-ch": 447,
	"./fr-ch.js": 447,
	"./fr.js": 448,
	"./fy": 449,
	"./fy.js": 449,
	"./gd": 450,
	"./gd.js": 450,
	"./gl": 451,
	"./gl.js": 451,
	"./he": 452,
	"./he.js": 452,
	"./hi": 453,
	"./hi.js": 453,
	"./hr": 454,
	"./hr.js": 454,
	"./hu": 455,
	"./hu.js": 455,
	"./hy-am": 456,
	"./hy-am.js": 456,
	"./id": 457,
	"./id.js": 457,
	"./is": 458,
	"./is.js": 458,
	"./it": 459,
	"./it.js": 459,
	"./ja": 460,
	"./ja.js": 460,
	"./jv": 461,
	"./jv.js": 461,
	"./ka": 462,
	"./ka.js": 462,
	"./kk": 463,
	"./kk.js": 463,
	"./km": 464,
	"./km.js": 464,
	"./ko": 465,
	"./ko.js": 465,
	"./ky": 466,
	"./ky.js": 466,
	"./lb": 467,
	"./lb.js": 467,
	"./lo": 468,
	"./lo.js": 468,
	"./lt": 469,
	"./lt.js": 469,
	"./lv": 470,
	"./lv.js": 470,
	"./me": 471,
	"./me.js": 471,
	"./mi": 472,
	"./mi.js": 472,
	"./mk": 473,
	"./mk.js": 473,
	"./ml": 474,
	"./ml.js": 474,
	"./mr": 475,
	"./mr.js": 475,
	"./ms": 477,
	"./ms-my": 476,
	"./ms-my.js": 476,
	"./ms.js": 477,
	"./my": 478,
	"./my.js": 478,
	"./nb": 479,
	"./nb.js": 479,
	"./ne": 480,
	"./ne.js": 480,
	"./nl": 482,
	"./nl-be": 481,
	"./nl-be.js": 481,
	"./nl.js": 482,
	"./nn": 483,
	"./nn.js": 483,
	"./pa-in": 484,
	"./pa-in.js": 484,
	"./pl": 485,
	"./pl.js": 485,
	"./pt": 487,
	"./pt-br": 486,
	"./pt-br.js": 486,
	"./pt.js": 487,
	"./ro": 488,
	"./ro.js": 488,
	"./ru": 489,
	"./ru.js": 489,
	"./se": 490,
	"./se.js": 490,
	"./si": 491,
	"./si.js": 491,
	"./sk": 492,
	"./sk.js": 492,
	"./sl": 493,
	"./sl.js": 493,
	"./sq": 494,
	"./sq.js": 494,
	"./sr": 496,
	"./sr-cyrl": 495,
	"./sr-cyrl.js": 495,
	"./sr.js": 496,
	"./ss": 497,
	"./ss.js": 497,
	"./sv": 498,
	"./sv.js": 498,
	"./sw": 499,
	"./sw.js": 499,
	"./ta": 500,
	"./ta.js": 500,
	"./te": 501,
	"./te.js": 501,
	"./tet": 502,
	"./tet.js": 502,
	"./th": 503,
	"./th.js": 503,
	"./tl-ph": 504,
	"./tl-ph.js": 504,
	"./tlh": 505,
	"./tlh.js": 505,
	"./tr": 506,
	"./tr.js": 506,
	"./tzl": 507,
	"./tzl.js": 507,
	"./tzm": 509,
	"./tzm-latn": 508,
	"./tzm-latn.js": 508,
	"./tzm.js": 509,
	"./uk": 510,
	"./uk.js": 510,
	"./uz": 511,
	"./uz.js": 511,
	"./vi": 512,
	"./vi.js": 512,
	"./x-pseudo": 513,
	"./x-pseudo.js": 513,
	"./yo": 514,
	"./yo.js": 514,
	"./zh-cn": 515,
	"./zh-cn.js": 515,
	"./zh-hk": 516,
	"./zh-hk.js": 516,
	"./zh-tw": 517,
	"./zh-tw.js": 517
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 879;


/***/ },

/***/ 900:
/***/ function(module, exports) {

module.exports = ".header {\n    margin-bottom: 30px;\n    /*border-bottom: 1px solid yellow;\n    background-color: #e3d5b3;*/\n}\n"

/***/ },

/***/ 901:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 902:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 903:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 904:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 905:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 906:
/***/ function(module, exports) {

module.exports = ".orderlistrow {\n    cursor: pointer;\n}"

/***/ },

/***/ 907:
/***/ function(module, exports) {

module.exports = ".nav-item2 {\n    background-color: black;\n}\n\n.nav {\n    background-color: lawngreen;\n}\n\n.t {\n    padding: 5px;\n}"

/***/ },

/***/ 908:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 909:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 910:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 911:
/***/ function(module, exports) {

module.exports = "<app-login #c=\"child\"></app-login>\n<div class=\"container\">\n<!--  <div class=\"row\">\n    HEADER LOGO\n  </div>-->\n  <div class=\"row header\">\n    <div class=\"col-md-12\">\n      <nav role=\"navigation\">\n        <ul class=\"nav navbar-nav\">\n          <li role=\"presentation\"><a routerLink=\"/\">Home</a></li>\n          <li role=\"presentation\"><a routerLink=\"/product\">Product</a></li>\n        </ul>\n        <ul class=\"nav navbar-nav navbar-right\">\n          <li role=\"presentation\" *ngIf=\"!loggedIn()\"><a (click)=\"c.showLogin()\">Login</a></li>\n          <span dropdown *ngIf=\"loggedIn()\">\n            <a dropdownToggle href id=\"menu\">{{username}} <span class=\"caret\"></span></a>\n            <ul class=\"dropdown-menu\" dropdownMenu aria-labelledby=\"menu\">\n              <li role=\"menuitem\"><a class=\"dropdown-item\" routerLink=\"/user/orders\">Orders</a></li>\n              <li role=\"menuitem\"><a class=\"dropdown-item\" routerLink=\"/user/customer\">Customer data</a></li>\n              <li class=\"divider dropdown-divider\"></li>\n              <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"logout()\">Logout</a></li>\n            </ul>\n          </span>\n        </ul>\n      </nav>\n    </div>\n  </div>\n  <div class=\"row\">\n    <router-outlet></router-outlet>\n  </div>\n</div>"

/***/ },

/***/ 912:
/***/ function(module, exports) {

module.exports = "<div bsModal #loginModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"hideLogin()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n        <h4 class=\"modal-title\">Login</h4>\n      </div>\n      <div class=\"modal-body\">\n        <form (ngSubmit)=\"login()\" #f=\"ngForm\">\n          <div class=\"form-group\">\n            <label for=\"pw\">E-Mail:</label>\n            <input type=\"email\" class=\"form-control\" required placeholder=\"email\" name=\"email\" [(ngModel)]=\"email\" />\n          </div>\n          <div class=\"form-group\">\n            <label for=\"pw\">Password:</label>\n            <input type=\"password\" class=\"form-control\" required placeholder=\"password\" id=\"pw\" name=\"pw\" [(ngModel)]=\"pw\" />\n          </div>\n          <button type=\"submit\" [disabled]=\"!f.form.valid\">submit</button>\n          <label *ngIf=\"lerror\">Login failed</label>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ },

/***/ 913:
/***/ function(module, exports) {

module.exports = "<div class=\"form-group\">\n  <label for=\"mail\" *ngIf=\"msgexists\">{{ msgexists }}</label>\n  <label for=\"mail\">email:</label>\n  <input type=\"email\" class=\"form-control\" id=\"mail\" [(ngModel)]=\"mail\" name=\"mail\" />\n</div>\n<div class=\"form-group\">\n  <label for=\"pw\">password:</label>\n  <input type=\"password\" class=\"form-control\" id=\"pw\" [(ngModel)]=\"pw1\" name=\"pw1\" />\n</div>\n<div class=\"form-group\">\n  <label for=\"pw2\">password (retype):</label>\n  <input type=\"password\" class=\"form-control\" id=\"pw2\" [(ngModel)]=\"pw2\" name=\"pw2\" />\n</div>"

/***/ },

/***/ 914:
/***/ function(module, exports) {

module.exports = "\n         <div class=\"form-group\">\n           <label for=\"firstname\">firstname:</label>\n           <input type=\"text\" class=\"form-control\" id=\"firstname\" required [(ngModel)]=\"customer.firstname\" name=\"firstname\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"lastname\">lastname:</label>\n           <input type=\"text\" class=\"form-control\" id=\"lastname\" required [(ngModel)]=\"customer.lastname\" name=\"lastname\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"street\">street:</label>\n           <input type=\"text\" class=\"form-control\" id=\"street\" required [(ngModel)]=\"customer.street\" name=\"street\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"housenumber\">number:</label>\n           <input type=\"number\" class=\"form-control\" id=\"housenumber\" required [(ngModel)]=\"customer.housenumber\" name=\"housenumber\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"zip\">zip:</label>\n           <input type=\"text\" class=\"form-control\" id=\"zip\" required [(ngModel)]=\"customer.zip\" name=\"zip\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"city\">city:</label>\n           <input type=\"text\" class=\"form-control\" id=\"city\" required [(ngModel)]=\"customer.city\" name=\"city\" />\n         </div>\n         <div class=\"form-group\">\n           <label for=\"phone\">phone:</label>\n           <input type=\"tel\" class=\"form-control\" id=\"phone\" required [(ngModel)]=\"customer.phone\" name=\"phone\" />\n         </div>\n"

/***/ },

/***/ 915:
/***/ function(module, exports) {

module.exports = "<alert type=\"success\" *ngIf=\"saved\">\n  <strong>Saved!</strong> Customer data saved\n</alert>\n<alert type=\"danger\" *ngIf=\"error\">\n  <strong>Error</strong> Customer data could not be saved\n</alert>\n<alert type=\"warning\" *ngIf=\"nocustomer\">\n  <strong>Error</strong> No customer data saved!\n</alert>\n<form (ngSubmit)=\"save()\" #f=\"ngForm\">\n  <app-customer></app-customer>\n  <button type=\"submit\" [disabled]=\"!f.form.valid\">save</button>\n</form>"

/***/ },

/***/ 916:
/***/ function(module, exports) {

module.exports = "<div class=\"col-md-9\">\n  <div class=\"row\">\n    <div class=\"col-md-9\"><h2>Order #{{order.id}} ({{order.created}})</h2></div>\n  </div>\n  <div class=\"row\" *ngFor=\"let item of order.items\">\n    <div class=\"col-md-9\">\n      <table class=\"table table-striped\">\n        <tr>\n          <th>Name</th>\n          <th># items</th>\n          <th>Price</th>\n        </tr>\n        <tr class=\"orderlistrow\" *ngFor=\"let o of order.items\">\n          <td>{{o.name}}</td>\n          <td>{{o.quantity}}</td>\n          <td>{{o.price | currency: 'EUR'}}</td>\n        </tr>\n        <tr>\n          <td>Total:</td>\n          <td>{{order.nitems}}</td>\n          <td>{{order.totalPrice | currency:'EUR'}}</td>\n        </tr>\n      </table>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-9\">\n      <table class=\"table\">\n        <tr><td colspan=\"2\"><h4>Customer information</h4></td></tr>\n        <tr>\n          <td>Firstname:</td>\n          <td>{{order.customer.firstname}}</td>\n        </tr>\n        <tr>\n          <td>Lastname:</td>\n          <td>{{order.customer.lastname}}</td>\n        </tr>\n        <tr>\n          <td>Street:</td>\n          <td>{{order.customer.street}}</td>\n        </tr>\n        <tr>\n          <td>Housenumber:</td>\n          <td>{{order.customer.housenumber}}</td>\n        </tr>\n        <tr>\n          <td>Zip:</td>\n          <td>{{order.customer.zip}}</td>\n        </tr>\n        <tr>\n          <td>City:</td>\n          <td>{{order.customer.city}}</td>\n        </tr>\n        <tr>\n          <td>Phone:</td>\n          <td>{{order.customer.phone}}</td>\n        </tr>\n      </table>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-9\"><button routerLink=\"/user/orders\">back</button></div>\n  </div>\n</div>"

/***/ },

/***/ 917:
/***/ function(module, exports) {

module.exports = "<div class=\"col-md-9\">\n  <alert class=\"warning\" *ngIf=\"noorders\">\n    <strong>No Orders</strong>\n  </alert>\n  <table class=\"table table-striped\" *ngIf=\"!noorders\">\n    <tr>\n      <th>Date</th>\n      <th># items</th>\n      <th>price</th>\n    </tr>\n    <tr *ngFor=\"let o of orders\" (click)=\"showOrder(o)\" class=\"orderlistrow\">\n      <td>{{o.created}}</td>\n      <td>{{o.nitems}}</td>\n      <td>{{o.totalPrice | currency: 'EUR'}}</td>\n    </tr>\n  </table>\n</div>"

/***/ },

/***/ 918:
/***/ function(module, exports) {

module.exports = "<alert type=\"danger\" *ngIf=\"error\">\n  <strong>Error</strong> Please try again later\n</alert>\n<!-- TODO later: customize class nav nav-tabs -->\n<div class=\"col-md-8 col-md-offset-2\">\n  <alert type=\"success\" *ngIf=\"done\">\n    <strong>Order sent</strong> Thanks for ordering!\n  </alert>\n  <tabset *ngIf=\"!done\">\n    <tab [active]=\"tabs[0].active\" (select)=\"tabs[0].active = true; tabs[1].active = false\" heading=\"your order\" class=\"t\">\n      <table class=\"table\">\n        <tr>\n          <th>Product</th>\n          <th>Price</th>\n          <th>Total</th>\n        </tr>\n        <tr *ngFor=\"let o of orders\">\n          <td>{{o.product.name}}</td>\n          <td>{{o.quantity}}x{{o.product.price | currency: 'EUR'}}</td>\n          <td>{{o.quantity * o.product.price | currency:'EUR'}}</td>\n        </tr>\n        <tr>\n          <td></td>\n          <td></td>\n          <td>Total: {{total | currency: 'EUR'}}</td>\n        </tr>\n      </table>\n      <button routerLink=\"/product\"><span class=\"glyphicon glyphicon-chevron-left\"></span>back</button>\n      <button (click)=\"switchTabs()\">next<span class=\"glyphicon glyphicon-chevron-right\"></span></button>\n    </tab>\n    <tab [disabled]=\"tabs[1].disabled\" [active]=\"tabs[1].active\" (select)=\"tabs[1].active = true; tabs[0].active = false\" heading=\"delivery information\">\n      <form (ngSubmit)=\"orderNow()\" #f=\"ngForm\">\n        <app-customer></app-customer>\n         <div class=\"form-group\" *ngIf=\"!loggedIn()\">\n           <div class=\"checkbox\">\n            <label>\n              <input type=\"checkbox\" [(ngModel)]=\"register\" name=\"register\" />\n              Register?\n            </label>\n           </div>\n         </div>\n         <div *ngIf=\"register\">\n          <app-register></app-register>\n         </div>\n         <button formnovalidate type=\"button\" (click)=\"switchTabs()\"><span class=\"glyphicon glyphicon-chevron-left\"></span>back</button>\n         <button type=\"submit\" [disabled]=\"!f.form.valid\">Submit</button>\n        </form>\n    </tab>\n  </tabset>\n</div>\n"

/***/ },

/***/ 919:
/***/ function(module, exports) {

module.exports = "<p>\n  Product-detail works!\n</p>\n"

/***/ },

/***/ 920:
/***/ function(module, exports) {

module.exports = "<div class=\"col-md-9\">\n  <p *ngIf=\"errorMessage\">{{errorMessage}}</p>\n  <h2>Pizza</h2>\n  <div class=\"row\" *ngFor=\"let product of products\">\n    <div class=\"col-md-12\">{{product.name}}\n      <div class=\"row\">\n        <div class=\"col-md-3\">PICTURE</div>\n        <div class=\"col-md-9\">\n          <span class=\"label label-primary\" style=\"margin-left: 5px;\" *ngFor=\"let comp of product.components\">{{comp.name}}</span>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-2 col-md-offset-10\">\n          {{product.price | currency: 'EUR'}}\n          <input #quantity type=\"number\" maxlength=\"2\" min=\"1\" max=\"9\" size=\"1\" value=\"1\" pattern=\"[1-9]{1}\" />\n          <button (click)=\"addToCart(product, quantity.value)\"><span class=\"glyphicon glyphicon-plus\"></span></button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"col-md-3\">\n  <shopping-cart></shopping-cart>\n</div>"

/***/ },

/***/ 921:
/***/ function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-12\"><h2>Shopping Cart</h2></div>\n</div>\n<div class=\"row\" *ngFor=\"let item of items\">\n  <div class=\"col-md-4\">{{item.quantity}}x{{item.product.name}}</div>\n  <div class=\"col-md-3\">{{item.product.price * item.quantity | currency: 'EUR'}}</div>\n  <div class=\"col-md-3\"><span class=\"glyphicon glyphicon-remove-sign\" (click)=\"delete(item)\"></span></div>\n</div>\n<div class=\"row\">\n  <div *ngIf=\"items.length != 0\" class=\"col-md-6\">Total: {{total | currency: 'EUR'}}</div>\n  <div *ngIf=\"items.length == 0\" class=\"col-md-6\">No Items</div>\n</div>\n<div class=\"row\">\n  <div class=\"col-md-6\"><button routerLink=\"/order\" [disabled]=\"items.length < 1\">Order now</button></div>\n</div>"

/***/ },

/***/ 954:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(593);


/***/ }

},[954]);
//# sourceMappingURL=main.bundle.map