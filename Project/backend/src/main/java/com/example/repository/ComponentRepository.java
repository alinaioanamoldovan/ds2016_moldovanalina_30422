package com.example.repository;


import com.example.model.ComponentP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComponentRepository extends JpaRepository<ComponentP, Long> {
    
}
