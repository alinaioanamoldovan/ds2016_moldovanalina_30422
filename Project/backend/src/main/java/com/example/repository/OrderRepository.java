package com.example.repository;


import com.example.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {
    
    @Query("select o from _Order o where o.user.mail = ?#{authentication.name}")
    List<Order> getOrdersFromCurrentUser();
    
    Optional<Order> findOneById(Long id);
}
