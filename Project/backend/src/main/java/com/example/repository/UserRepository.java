package com.example.repository;


import com.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    
    //@Query("select u from User u where u.customer.mail = ?1")
    Optional<User> findOneByMail(String email);
    
    Optional<User> findOneById(Long id);
    
    @Query("select u from _User u where u.mail = ?#{principal.username}")
    Optional<User> findCurrentUser();
}
