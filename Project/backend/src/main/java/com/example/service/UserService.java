package com.example.service;


import com.example.model.User;
import com.example.repository.AuthorityRepository;
import com.example.repository.CustomerRepository;
import com.example.repository.UserRepository;
import com.example.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;

@Service
@Transactional
public class UserService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private AuthorityRepository authorityRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Transactional
    public User registerUser(User user) {
        userRepository.findOneByMail(user.getMail()).ifPresent(x -> {
            throw new EmailAlreadyRegisterdException();
        });
        
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAuthorities(Arrays.asList(authorityRepository.findByName("USER")));
        User u = userRepository.saveAndFlush(user);
        return u;
    }
    
    public User findCurrentUser() {
        return userRepository.findOneByMail(SecurityUtil.getCurrentUsername()).get();
    }
}
