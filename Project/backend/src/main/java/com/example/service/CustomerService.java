package com.example.service;


import com.example.controller.CustomerDTO;
import com.example.converter.CustomerConverter;
import com.example.model.Customer;
import com.example.model.User;
import com.example.repository.CustomerRepository;
import com.example.repository.UserRepository;
import com.example.security.SecurityUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class CustomerService {
    @Autowired
    private Logger log;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CustomerConverter customerConverter;

    @Transactional
    public Customer createCustomer(Customer customer) {
        log.debug(customer.toString());
        User u = userRepository.findOneByMail(SecurityUtil.getCurrentUsername())
                .orElseThrow(() -> new UserNotFoundException());
        Customer c = customerRepository.saveAndFlush(customer);
        u.setCustomer(c);
        userRepository.saveAndFlush(u);
        
        return c;
    }

    public CustomerDTO getCurrent() {
        User u = userRepository.findOneByMail(SecurityUtil.getCurrentUsername())
                .orElseThrow(() -> new UserNotFoundException());
        
        Customer customer = u.getCustomer();
        if(customer == null)
            throw new CustomerNotFoundException();

        log.info(customer.toString());
        return customerConverter.convert(customer);
    }
}
