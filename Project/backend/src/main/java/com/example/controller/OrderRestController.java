package com.example.controller;




import com.example.model.Order;
import com.example.service.OrderService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/api")
public class OrderRestController {

    @Autowired
    private Logger log;

    @Autowired
    private OrderService orderService;

    @RequestMapping(path = "/order", method = RequestMethod.POST)
    public ResponseEntity<Order> saveOrder(@RequestBody OrderDTO order) {
        log.debug("TEST: {}", order);

        Order createdOrder = orderService.createOrder(order);

        return ResponseEntity.created(URI.create("/api/order/" + createdOrder.getId())).build();
    }

    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    @RequestMapping(path = "/order", method = RequestMethod.GET)
    public ResponseEntity<List<OrderDTO>> getOrders() {
        List<OrderDTO> l = orderService.getOrders();
        return ResponseEntity.ok(l);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    @RequestMapping(path = "/order/{id}", method = RequestMethod.GET)
    public ResponseEntity<OrderDTO> getOrderDetails(@PathVariable String id) {
        OrderDTO orderDTO = orderService.getOrderDetails(id);

        return ResponseEntity.ok(orderDTO);
    }
}
