package com.example.controller;



import com.example.model.ComponentP;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class ComponentRestController {
    
    @RequestMapping(path = "/component/{name}", method = RequestMethod.GET)
    public ComponentP getIngredient(@PathVariable String name) {
        return null;
    }
    
    @RequestMapping(path = "/admin/component", method = RequestMethod.POST)
    public ResponseEntity<ComponentP> add(ComponentP component) {
        return null;
    }
    
}
