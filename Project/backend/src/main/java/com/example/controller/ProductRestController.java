package com.example.controller;

import com.example.model.Product;
import com.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class ProductRestController {
    
    @Autowired
    private ProductService productService;
    
    @RequestMapping(path = "/products", method = RequestMethod.GET)
    public List<Product> getAll() {
        
        return productService.findAll();
    }
    
    @RequestMapping(path = "/admin/product", method = RequestMethod.POST)
    public ResponseEntity<Product> add(Product product) {
        return null;
    }
}
