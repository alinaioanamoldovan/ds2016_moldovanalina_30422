package com.example.converter;


import com.example.controller.OrderItemDTO;
import com.example.model.OrderItem;
import com.example.model.Product;
import com.example.repository.ProductRepository;
import com.example.service.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OrderItemDTOConverter implements Converter<OrderItemDTO, OrderItem> {

    @Autowired
    private ProductRepository productRepository;
    
    @Override
    public OrderItem convert(OrderItemDTO orderitem) {
        OrderItem oi = new OrderItem();
        oi.setQuantity(orderitem.getQuantity());
        
        Product p = productRepository.findOneByOrdernumber(orderitem.getOrdernumber()).orElseThrow(() -> new ProductNotFoundException());
        oi.setProduct(p);
        return oi;
    }
    
}
