package com.example.converter;




import com.example.controller.OrderItemDTO;
import com.example.model.OrderItem;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class OrderItemConverter implements Converter<OrderItem, OrderItemDTO> {

    @Override
    public OrderItemDTO convert(OrderItem oi) {
        OrderItemDTO oidto = new OrderItemDTO();
        oidto.setQuantity(oi.getQuantity());
        oidto.setName(oi.getProduct().getName());
        oidto.setPrice(oi.getProduct().getPrice());
        oidto.setOrdernumber(oi.getProduct().getOrdernumber());
        
        return oidto;
    }
    
}
