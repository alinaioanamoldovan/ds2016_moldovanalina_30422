package com.example.converter;


import com.example.controller.CustomerDTO;
import com.example.model.Customer;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerConverter implements Converter<Customer, CustomerDTO> {

    @Override
    public CustomerDTO convert(Customer source) {
        return new CustomerDTO(
                source.getFirstname(),
                source.getLastname(),
                source.getStreet(),
                source.getHousenumber(),
                source.getZip(),
                source.getCity(),
                source.getPhone());
    }
    
}
