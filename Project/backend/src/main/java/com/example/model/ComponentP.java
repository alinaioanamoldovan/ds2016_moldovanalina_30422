package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Toshiba on 2/15/2017.
 */
@Entity
public class ComponentP {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String description;

    public ComponentP() {
    }

    public ComponentP(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Component{" + "id=" + id + ", name=" + name + ", description=" + description + '}';
    }
}
