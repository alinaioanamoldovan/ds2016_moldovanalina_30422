package com.example.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Toshiba on 2/15/2017.
 */
@Entity
public class Product {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String ordernumber;

    @NotNull
    private String name;

    @Digits(integer = 4, fraction = 2)
    private double price;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<ComponentP> components;

    public Product() {
    }

    public Product(String ordernumber, String name, double price) {
        this.ordernumber = ordernumber;
        this.name = name;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<ComponentP> getIngredients() {
        return components;
    }

    public void setIngredients(List<ComponentP> ingredients) {
        this.components = components;
    }
}
